﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Constants
{
    public static class MonitoreoControllerRoute
    {
        public const string GetJqGridHandler = ControllerName.Monitoreo + "GetJqGridHandler";
        public const string GetInfoCliente = ControllerName.Monitoreo + "GetInfoCliente";
        public const string GetAlerta = ControllerName.Monitoreo + "GetAlerta";
        public const string GetDetalleAlerta = ControllerName.Monitoreo + "GetDetalleAlerta";
        public const string GetHistorico = ControllerName.Monitoreo + "GetHistorico";
        public const string GetColasAsignadas = ControllerName.Monitoreo + "GetColasAsignadas";
        public const string RegistrarGestion = ControllerName.Monitoreo + "RegistrarGestion";
    }
}