﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Constants
{
    public static class MonitoreoControllerAction
    {
        public const string Index = "Index";
        public const string InfoCliente = "InfoCliente";
        public const string Alerta = "Alerta";
        public const string DetalleAlerta = "DetalleAlerta";
        public const string Historico = "Historico";
        public const string ColasAsignadas = "ColasAsignadas";
        public const string RegistrarGestion = "RegistrarGestion";
    }
}