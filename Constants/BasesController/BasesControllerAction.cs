﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Constants
{
    public static class BasesControllerAction
    {
        public const string Index = "Index";
        public const string MakeDropdown = "MakeDropdown";
        public const string JqGridDispatcher = "JqGridDispatcher";
        public const string ListarProductos = "ListarProductos";
        public const string GuardarBloqueo = "GuardarBloqueo";
        public const string GuardarTrx = "GuardarTrx";
        public const string GuardarNovedad = "GuardarNovedad";
        public const string GuardarFraude = "GuardarFraude";
        public const string ExportarExcel = "ExportarExcel";
        public const string IngresoMasivoExcel = "IngresoMasivoExcel";

    }
}