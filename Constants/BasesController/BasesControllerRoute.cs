﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Constants
{
    public static class BasesControllerRoute
    {
        public const string JqGridDispatcher = ControllerName.Bases + "JqGridDispatcher";
        public const string ListarProductos = ControllerName.Bases + "ListarProductos";
        public const string MakeDropdown = ControllerName.Bases + "MakeDropdown";
        public const string GuardarBloqueo = ControllerName.Bases + "GuardarBloqueo";
        public const string GuardarTrx = ControllerName.Bases + "GuardarTrx";
        public const string GuardarNovedad = ControllerName.Bases + "GuardarNovedad";
        public const string GuardarFraude = ControllerName.Bases + "GuardarFraude";
        public const string ExportarExcel = ControllerName.Bases + "ExportarExcel";
        public const string IngresoMasivo = ControllerName.Bases + "IngresoMasivo";
    }
}