﻿namespace SIIGMA.Constants
{
    public static class ContentDeliveryNetwork
    {
        public static class Google
        {
            public const string Domain = "ajax.googleapis.com";
            public const string JQueryUrl = "//ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js";
            public const string FontDomain = "fonts.gstatic.com";
            public const string FontDomain2 = "fonts.googleapis.com";
            public const string RobotoCondensedUrl = "//fonts.googleapis.com/css?family=Roboto+Condensed";
        }

        public static class MaxCdn
        {
            public const string Domain = "maxcdn.bootstrapcdn.com";
            public const string FontAwesomeUrl = "//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css";
        }

        public static class Microsoft
        {
            public const string Domain = "ajax.aspnetcdn.com";
            public const string JQueryValidateUrl = "//ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js";
            public const string JQueryValidateUnobtrusiveUrl = "//ajax.aspnetcdn.com/ajax/mvc/5.2.3/jquery.validate.unobtrusive.min.js";
            public const string ModernizrUrl = "//ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.8.3.js";
            public const string BootstrapUrl = "//ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/bootstrap.min.js";
        }

        public static class CloudFlare
        {
            public const string Domain = "cdnjs.cloudflare.com";
            public const string jsGridUrl = "//cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css";
            public const string jsGridThemeUrl = "//cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css";
            public const string jsGridJsUrl = "//cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js";
        }
    }
}