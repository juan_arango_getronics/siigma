﻿namespace SIIGMA.Constants
{
    public class Application
    {
        public const string Name = "Sistema Integrado de Información para Gestión y Monitoreo de Alertas";
        public const string ShortName = "SIIGMA";
        public const string Title = "SIIGMA - Bancolombia";
    }
}