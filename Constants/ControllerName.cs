﻿namespace SIIGMA.Constants
{
    public static class ControllerName
    {
        public const string Error = "Error";
        public const string Home = "Home";
        public const string Config = "Config";
        public const string Monitoreo = "Monitoreo";
        public const string Tiempos = "Tiempos";
        public const string Reglas = "Reglas";
        public const string Bases = "Bases";
    }
}