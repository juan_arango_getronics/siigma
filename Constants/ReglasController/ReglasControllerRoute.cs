﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Constants
{
    public static class ReglasControllerRoute
    {
        public const string AlmacenaBases = ControllerName.Reglas + "AlmacenaBases";
        public const string RegistraReceptor = ControllerName.Reglas + "RegistraReceptor";
    }
}