﻿namespace SIIGMA.Constants
{
    public static class ConfigControllerRoute
    {
        public const string GetIndex = ControllerName.Config + "GetIndex";
        public const string GetUsernames = ControllerName.Config + "GetUsernames";
        public const string GetAllRoles = ControllerName.Config + "GetAllRoles";
        public const string EliminarUsuario = ControllerName.Config + "EliminarUsuario";
        public const string AgregarUsuario = ControllerName.Config + "AgregarUsuario";
        public const string GetColas = ControllerName.Config + "GetColas";
        public const string AgregarCola = ControllerName.Config + "AgregarCola";
        public const string EliminarCola = ControllerName.Config + "EliminarCola";
        public const string GetMaestrosSelect2 = ControllerName.Config + "GetMaestrosSelect2";
        public const string GetColasReglasUsuarios = ControllerName.Config + "GetColasReglasUsuarios";
        public const string GuardarReglas = ControllerName.Config + "GuardarReglas";
        public const string GuardarUsuario = ControllerName.Config + "GuardarUsuario";
        public const string EliminarReglaCola = ControllerName.Config + "EliminarReglaCola";
        public const string EliminarUsuarioCola = ControllerName.Config + "EliminarUsuarioCola";
        public const string GetColasTabla = ControllerName.Config + "GetColasTabla";
    }
}