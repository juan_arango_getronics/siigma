﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("SIIGMA_Roles")]
    public class Role
    {
        [Key]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}