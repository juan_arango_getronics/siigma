﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("SIIGMA_Queues")]
    public class Queues
    {
        [Key]
        public string QueueName { get; set; }
        public string Description { get; set; }
    }
}