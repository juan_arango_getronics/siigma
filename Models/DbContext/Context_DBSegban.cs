﻿using System.Data.Entity;

namespace SIIGMA.Models
{
    public class Context_DBSegban : DbContext
    {
        public Context_DBSegban() : base("connSegban") { }

        public virtual DbSet<UserRoles> TblUserRoles { get; set; }
        public virtual DbSet<UserQueues> TblUserQueues { get; set; }
        public virtual DbSet<Queues> TblQueues { get; set; }
        public virtual DbSet<ReglaQueues> TblReglaQueues { get; set; }
        public virtual DbSet<Reglas> TblReglas { get; set; }
        public virtual DbSet<TiemposUsuario> TblTiemposUsuario { get; set; }
        public virtual DbSet<MapeoBases> TblMapeoClases { get; set; }
        public virtual DbSet<InfoProducto> TblInfoProducto { get; set; }
        public virtual DbSet<InfoTransaccion> TblInfoTransaccion { get; set; }
        public virtual DbSet<NovedadTransaccion> TblNovedadTransaccion { get; set; }
        public virtual DbSet<MarcacionFraude> TblMarcacionFraude { get; set; }
        public virtual DbSet<TipoBloqueo> TblTipoBloqueo { get; set; }
        public virtual DbSet<Producto> TblProducto { get; set; }
        public virtual DbSet<EstadoTransaccion> TblEstadoTransaccion { get; set; }
        public virtual DbSet<Canal> TblCanal { get; set; }
        public virtual DbSet<TipoTransaccion> TblTipoTransaccion { get; set; }
        public virtual DbSet<Ciudad> TblCiudad { get; set; }
        public virtual DbSet<Pais> TblPais { get; set; }
        public virtual DbSet<RespuestaTxa> TblRespuestaTxa { get; set; }
        public virtual DbSet<HerramientaDeteccion> TblHerramientaDeteccion { get; set; }
        public virtual DbSet<RiesgoAlerta> TblRiesgoAlerta { get; set; }
        public virtual DbSet<CausasDesmarcacion> TblCausasDesmarcacion { get; set; }
        public virtual DbSet<Empleado> TblEmpleado { get; set; }
        public virtual DbSet<Oficina> TblOficina { get; set; }
        public virtual DbSet<Usuario> TblUsuario { get; set; }
        public virtual DbSet<Modalidad> TblModalidad { get; set; }
        public virtual DbSet<Role> TblRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.TIPO_PRODUCTO).HasColumnName("TIPO PRODUCTO");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.NUMERO_PRODUCTO).HasColumnName("NUMERO PRODUCTO");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.NUMERO_CUENTA).HasColumnName("NUMERO DE CUENTA");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.USUARIO_CONFIRMACION).HasColumnName("USUARIO CONFIRMACION");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.CC_CONFIRMACION).HasColumnName("CC CONFIRMACION");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.OFICINA_CONFIRMACION).HasColumnName("OFICINA CONFIRMACION");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.FECHA_FRAUDE).HasColumnName("FECHA FRAUDE");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.VALOR_FRAUDE).HasColumnName("VALOR FRAUDE");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.VALOR_PROTEGIDO).HasColumnName("VALOR PROTEGIDO");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.VALOR_POR_RECUPERAR).HasColumnName("VALOR POR RECUPERAR");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.FECHA_INGRESO).HasColumnName("FECHA INGRESO");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.HORA_INGRESO).HasColumnName("HORA INGRESO");
            modelBuilder.Entity<MarcacionFraude>().Property(q => q.USUARIO_INGRESO).HasColumnName("USUARIO INGRESO");

            modelBuilder.Entity<Pais>().Property(q => q.Codigo_Pais).HasColumnName("Código Pais");
            modelBuilder.Entity<Pais>().Property(q => q.Cod_Area).HasColumnName("Cód Area");
            modelBuilder.Entity<Pais>().Property(q => q.NPais).HasColumnName("País");
        }
    }
}