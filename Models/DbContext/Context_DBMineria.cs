﻿using System.Data.Entity;

namespace SIIGMA.Models
{
    public class Context_DBMineria : DbContext
    {
        public Context_DBMineria() : base("connMineria") { }

        public virtual DbSet<Clientes_Demo_2015> TblClientesDemo2015 { get; set; }
        public virtual DbSet<Clientes_Grupo_2015> TblClientesGrupo2015 { get; set; }
        public virtual DbSet<Clientes_Demo> TblClientesDemo { get; set; }
        public virtual DbSet<ConsolidadoAlertas> TblConsolidadoAlertas { get; set; }
        public virtual DbSet<Vista_Historico> TblVistaHistorico { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}