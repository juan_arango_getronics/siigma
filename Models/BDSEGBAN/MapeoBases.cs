﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.SIIGMA_MapeoBases")]
    public class MapeoBases
    {
        [Key]
        [Column(Order = 0)]
        public string Regla { get; set; }
        [Key]
        [Column(Order = 1)]
        public string Modelo { get; set; }
        public string CampoOrigen { get; set; }
        [Key]
        [Column(Order = 2)]
        public string CampoDestino { get; set; }
        public string ValorPorDefecto { get; set; }
    }
}