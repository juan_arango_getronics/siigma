﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Paises")]
    public partial class Pais
    {
        [Key]
        public String Codigo_Pais { get; set; }
        public String Cod_Area { get; set; }
        public String NPais { get; set; }
    }
}