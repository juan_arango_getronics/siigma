﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Causas_Desmarcacion")]
    public partial class CausasDesmarcacion
    {
        [Key]
        public String CAUSA_DESMARCACION { get; set; }
    }
}