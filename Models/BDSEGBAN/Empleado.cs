﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Empleados")]
    public partial class Empleado
    {
        [Key]
        public String CEDULA { get; set; }
        public String NOMBRE { get; set; }
        public String CARGO { get; set; }
    }
}