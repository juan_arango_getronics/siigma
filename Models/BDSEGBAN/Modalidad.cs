﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Modalidad")]
    public partial class Modalidad
    {
        [Key]
        public String MODALIDAD { get; set; }
    }
}