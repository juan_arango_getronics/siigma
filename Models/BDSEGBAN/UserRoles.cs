﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.SIIGMA_UserRoles")]
    public class UserRoles
    {
        public UserRoles(string user, string rol)
        {
            this.UserName = user;
            this.RoleName = rol;
        }

        public UserRoles() { }

        [Key]
        [Column(Order = 0)]
        public string UserName { get; set; }
        [Key]
        [Column(Order = 1)]
        public string RoleName { get; set; }
    }
}