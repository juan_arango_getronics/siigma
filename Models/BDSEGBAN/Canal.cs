﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Canal")]
    public partial class Canal
    {
        [Key]
        public String CANAL { get; set; }
        public String NOMBRE_CANAL { get; set; }
    }
}