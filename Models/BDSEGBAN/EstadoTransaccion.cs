﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Estado_Transaccion")]
    public partial class EstadoTransaccion
    {
        [Key]
        public String MARCACION_TXA { get; set; }
    }
}