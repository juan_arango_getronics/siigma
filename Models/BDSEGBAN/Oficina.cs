﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Oficinas")]
    public partial class Oficina
    {
        [Key]
        public String CODIGO { get; set; }
        public String NOMBRE_OFICINA { get; set; }
        public String CIUDAD { get; set; }
        public String DEPARTAMENTO { get; set; }
    }
}