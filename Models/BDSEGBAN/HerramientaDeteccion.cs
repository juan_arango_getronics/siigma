﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Herramientas_Deteccion")]
    public partial class HerramientaDeteccion
    {
        [Key]
        public String HERRAMIENTA_DETECCION { get; set; }
    }
}