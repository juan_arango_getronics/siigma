﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Producto")]
    public partial class Producto
    {
        [Key]
        public String TIPO_PRODUCTO { get; set; }
        public String NOMBRE_PRODUCTO { get; set; }
    }
}