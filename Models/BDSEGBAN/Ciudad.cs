﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("CAJEROS.tbl_ciudad")]
    public partial class Ciudad
    {
        [Key]
        public int codCiudad { get; set; }
        public String ciudad { get; set; }
        public String depto { get; set; }
        public String region { get; set; }
    }
}