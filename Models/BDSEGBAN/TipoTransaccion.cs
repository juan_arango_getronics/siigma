﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Transacciones")]
    public partial class TipoTransaccion
    {
        [Key]
        public String TIPO_TRANSACCION { get; set; }
    }
}