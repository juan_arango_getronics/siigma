﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Respuesta_TXA")]
    public partial class RespuestaTxa
    {
        [Key]
        public String COD_RESPUESTA { get; set; }
        public String DESCRIPCION_RESPUESTA { get; set; }
    }
}