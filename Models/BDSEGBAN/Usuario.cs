﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Usuarios")]
    public partial class Usuario
    {
        public String CEDULA { get; set; }
        public String NOMBRE { get; set; }
        public String APELLIDO { get; set; }
        public String USUARIO_RED { get; set; }
        [Key]
        public String USUARIO_BASE { get; set; }
        public String ESTADO_DEL_USUARIO { get; set; }
        public String USUARIO_NACIONAL { get; set; }
        public String USUARIO_MEDELLIN { get; set; }
        public String USUARIO_LOTES_NOTES { get; set; }
        public String TELEFONO_CELULAR { get; set; }
        public String TELEFONO_CASA { get; set; }
    }
}