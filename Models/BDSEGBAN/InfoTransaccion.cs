﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Informacion_Transacciones")]
    public class InfoTransaccion
    {
        public InfoTransaccion() { }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ID_CLIENTE { get; set; }
        public string TIPO_PRODUCTO { get; set; }
        public string NUMERO_PRODUCTO { get; set; }
        public string NUMERO_CUENTA { get; set; }
        public System.DateTime FECHA_TXA { get; set; }
        public System.DateTime HORA_TXA { get; set; }
        public decimal VALOR_TXA { get; set; }
        public string CANAL_TXA { get; set; }
        public string NOMBRE_DISPOSITIVO { get; set; }
        public string TIPO_TXA { get; set; }
        public string CIUDAD_TXA { get; set; }
        public string PAIS_TXA { get; set; }
        public string RESPUESTA_TXA { get; set; }
        public string CUENTA_RECEPTORA { get; set; }
        public string IP_TXA { get; set; }
        public string RIESGO_ALERTA { get; set; }
        public string MODO_DETECCION { get; set; }
        public string ESTADO_TXA { get; set; }
        public System.DateTime FECHA_INGRESO { get; set; }
        public System.DateTime HORA_INGRESO { get; set; }
        public string USUARIO_INGRESO { get; set; }
        public string Flag_Etdo { get; set; }
    }
}