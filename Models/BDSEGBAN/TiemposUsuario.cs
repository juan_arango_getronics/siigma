﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.SIIGMA_TiemposUsuario")]
    public class TiemposUsuario
    {
        public TiemposUsuario(string id, string usuario, string estado, DateTime fechainicio)
        {
            this.Id = id;
            this.NombreUsuario = usuario;
            this.Estado = estado;
            this.FechaInicio = fechainicio;
        }

        [Key]
        public string Id { get; set; }
        public string NombreUsuario { get; set; }
        public string Estado { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public System.DateTime FechaFinal { get; set; }
    }
}