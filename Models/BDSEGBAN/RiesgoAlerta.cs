﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Riesgo_Alerta")]
    public partial class RiesgoAlerta
    {
        [Key]
        public String RIESGO_ALERTA { get; set; }
    }
}