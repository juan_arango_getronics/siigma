﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.SIIGMA_ReglaQueues")]
    public class ReglaQueues
    {
        public ReglaQueues(string regla, string cola)
        {
            this.NombreRegla = regla;
            this.QueueName = cola;
        }

        public ReglaQueues() { }

        [Key]
        [Column(Order = 0)]
        public string NombreRegla { get; set; }
        [Key]
        [Column(Order = 1)]
        public string QueueName { get; set; }
    }
}