﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Marcacion_Fraude")]
    public partial class MarcacionFraude
    {
        [Key]
        [Column(Order = 0)]
        public string ID_CLIENTE { get; set; }
        [Key]
        [Column(Order = 1)]
        public string TIPO_PRODUCTO { get; set; }
        [Key]
        [Column(Order = 2)]
        public string NUMERO_PRODUCTO { get; set; }
        [Key]
        [Column(Order = 3)]
        public string NUMERO_CUENTA { get; set; }
        public string USUARIO_CONFIRMACION { get; set; }
        public string CC_CONFIRMACION { get; set; }
        public string OFICINA_CONFIRMACION { get; set; }
        public DateTime FECHA_FRAUDE { get; set; }
        public decimal VALOR_FRAUDE { get; set; }
        public decimal VALOR_PROTEGIDO { get; set; }
        public decimal VALOR_POR_RECUPERAR { get; set; }
        public string MODALIDA { get; set; }
        [Key]
        [Column(Order = 4)]
        public DateTime FECHA_INGRESO { get; set; }
        [Key]
        [Column(Order = 5)]
        public DateTime HORA_INGRESO { get; set; }
        public string USUARIO_INGRESO { get; set; }
        public DateTime? FECHA_CREACION { get; set; }
    }
}