﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using SIIGMA.Services;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Informacion_Producto")]
    public class InfoProducto
    {
        public InfoProducto() { }

        [Key]
        [Column(Order = 0)]
        public string ID_CLIENTE { get; set; }
        [Key]
        [Column(Order = 1)]
        public string TIPO_PRODUCTO { get; set; }
        [Key]
        [Column(Order = 2)]
        public string NUMERO_PRODUCTO { get; set; }
        [Key]
        [Column(Order = 3)]
        public string NUMERO_CUENTA { get; set; }
        public decimal SALDO_DISPONIBLE { get; set; }
        [Key]
        [Column(Order = 4)]
        public System.DateTime FECHA_INGRESO { get; set; }
        [Key]
        [Column(Order = 5)]
        public System.DateTime HORA_INGRESO { get; set; }
        public string USUARIO_MONITOREO { get; set; }
        public string TIPO_BLOQUEO { get; set; }
        public string OBSERVACIONES { get; set; }
        public string USUARIO_INGRESO { get; set; }
        public string CLIENTE_CONTACTADO { get; set; }
    }
}