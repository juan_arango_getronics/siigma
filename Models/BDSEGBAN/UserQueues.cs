﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.SIIGMA_UserQueues")]
    public class UserQueues
    {
        public UserQueues(string usuario, string cola)
        {
            this.UserName = usuario;
            this.QueueName = cola;
        }

        public UserQueues() { }

        [Key]
        [Column(Order = 0)]
        public string UserName { get; set; }
        [Key]
        [Column(Order = 1)]
        public string QueueName { get; set; }
    }
}