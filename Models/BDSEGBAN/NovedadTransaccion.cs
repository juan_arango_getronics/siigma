﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Novedad_Transacciones")]
    public partial class NovedadTransaccion
    {
        [Key]
        [Column(Order = 0)]
        public string ID_CLIENTE { get; set; }
        [Key]
        [Column(Order = 1)]
        public string TIPO_PRODUCTO { get; set; }
        [Key]
        [Column(Order = 2)]
        public string NUMERO_PRODUCTO { get; set; }
        [Key]
        [Column(Order = 3)]
        public string NUMERO_CUENTA { get; set; }
        public string USUARIO_DESMARCACION { get; set; }
        public string EMPLEADO_DESMARCACION { get; set; }
        public string OFICINA { get; set; }
        public string OBSERVACIONES { get; set; }
        [Key]
        [Column(Order = 4)]
        public DateTime FECHA_DESBLOQUEO { get; set; }
        [Key]
        [Column(Order = 5)]
        public DateTime HORA_DESBLOQUEO { get; set; }
        public string CAUSA_DESMARCACION { get; set; }
        public DateTime? FECHA_CREACION { get; set; }
    }
}