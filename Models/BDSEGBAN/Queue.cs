﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.SIIGMA_Queues")]
    public class Queues
    {
        public Queues(string name, string descripcion)
        {
            this.QueueName = name;
            this.Description = descripcion;
        }

        public Queues() { }

        [Key]
        public string QueueName { get; set; }
        public string Description { get; set; }
    }
}