﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("GESTION_DEBITO.Tbl_Tipo_Bloqueo")]
    public partial class TipoBloqueo
    {
        [Key]
        public String GESTION { get; set; }
    }
}