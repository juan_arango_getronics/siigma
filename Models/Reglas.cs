﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("SIIGMA_Reglas")]
    public class Reglas
    {
        [Key]
        public string Regla { get; set; }
        public string Grupo { get; set; }
    }
}