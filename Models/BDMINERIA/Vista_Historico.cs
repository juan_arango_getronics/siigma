﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("REGLAS.VISTA_MONIT")]
    public partial class Vista_Historico
    {
        [Key]
        public long DOCUMENTO { get; set; }
        public string REGLA { get; set; }
        public string GESTION { get; set; }
        public long FECHA { get; set; }
        public string HORA { get; set; }
        public string CANAL { get; set; }
        public string ALERTA_OBSERVACIONES { get; set; }
        public long RECEPTOR { get; set; }
        public string ALERTA_USUARIO { get; set; }
    }
}