﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SIIGMA.Models
{
    [Table("CONSOLIDADO_ALERTAS_NUEVO")]
    public partial class ConsolidadoAlertas
    {
        [Key]
        public String LLAVE { get; set; }
        public String ID { get; set; }
        public String DOCUMENTO { get; set; }
        public String REGLA { get; set; }
        public DateTime HORA { get; set; }
        public String GESTION { get; set; }
        public String USUARIO { get; set; }
    }
}