﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("MDL_SEGMENTACION.CLIENTES_GRUPO_2015")]
    public partial class Clientes_Grupo_2015
    {
        [Key]
        public Int64 DOCUMENTO { get; set; }
        public Int64 GRUPO { get; set; }
        public Int32 FCN_RFM { get; set; }
        public Int32 MOV_RFM { get; set; }
        public Int32 PSE_RFM { get; set; }
        public Int32 SVP_RFM { get; set; }
        public Int32 STF_RFM { get; set; }
        public Int32 ATM_RFM { get; set; }
        public Int32 CNB_RFM { get; set; }
        public Int32 PAC_RFM { get; set; }
        public Int32 POS_RFM { get; set; }
        public Int32 SUCURSAL_RFM { get; set; }
        public String NOMBRE_GRUPO { get; set; }
        public Int64 FECHA_EJEC { get; set; }
        public String HORA_EJEC { get; set; }
    }
}