﻿using System.Data.Entity;

namespace SIIGMA.Models
{
    public class AppDbSegbanContext : DbContext
    {
        public AppDbSegbanContext() : base("connSegban") { }
        public virtual DbSet<UserRoles> EUserRoles { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}