﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SIIGMA.Models
{
    [Table("MDL_SEGMENTACION.CLIENTES_DEMO_2015")]
    public partial class Clientes_Demo_2015
    {
        [Key]
        public Int64 DOCUMENTO { get; set; }
        public String TIPODOC { get; set; }
        public String NOMBRE { get; set; }
        public Int64 FECHA_EXPEDICION_CED { get; set; }
        public String CIUDAD { get; set; }
        public String DEPARTAMENTO { get; set; }
        public String REGION { get; set; }
        public Int32 FECHAACT { get; set; }
        public Int64 FECHA_VIN { get; set; }
        public Int64 OFICINA_VIN { get; set; }
        public String SEXO { get; set; }
        public String NOMBRE_PROFESION { get; set; }
        public String ESTADO_CIVIL { get; set; }
        public String NOM_OCUPACION { get; set; }
        public String SEGMENTO { get; set; }
        public Int64 EDAD { get; set; }
        public Int64 INGRESOS { get; set; }
        public Int64 EGRESOS { get; set; }
        public String LUGAR_TRABAJO { get; set; }
        public Int64 TELEFONO1 { get; set; }
        public Int64 TELEFONO2 { get; set; }
        public Int64 CELULAR2 { get; set; }
        public String CORREO_CIF { get; set; }
        public String DIR_RESIDENCIA_CIF { get; set; }
        public String DIR_LABORAL_CIF { get; set; }
        public String EMAIL_EMPRESA_CIF { get; set; }
        public String CORREO_ALYNOT { get; set; }
        public String CELULAR_ALYNOT { get; set; }
        public Int64 FECHA_EJEC { get; set; }
        public String HORA_EJEC { get; set; }
    }
}