﻿namespace SIIGMA
{
    using System.Web.Optimization;
    using SIIGMA.Constants;

    public static class BundleConfig
    {
        /// <summary>
        /// For more information on bundling, visit <see cref="http://go.microsoft.com/fwlink/?LinkId=301862"/>.
        /// </summary>
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Enable Optimizations
            // Set EnableOptimizations to false for debugging. For more information,
            // Web.config file system.web/compilation[debug=true]
            // OR
            // BundleTable.EnableOptimizations = true;

            // Enable CDN usage.
            // Note: that you can choose to remove the CDN if you are developing an intranet application.
            // Note: We are using Google's CDN where possible and then Microsoft if not available for better
            //       performance (Google is more likely to have been cached by the users browser).
            // Note: that protocol (http:) is omitted from the CDN URL on purpose to allow the browser to choose the protocol.
            bundles.UseCdn = true;

            AddCss(bundles);
            AddJavaScript(bundles);
        }

        private static void AddCss(BundleCollection bundles)
        {
            // Bootstrap - Twitter Bootstrap CSS (http://getbootstrap.com/).
            // Site - Your custom site CSS.
            // Note: No CDN support has been added here. Most likely you will want to customize your copy of bootstrap.
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap/site.css").Include("~/Content/site.css"));

            // Font Awesome - Icons using font (http://fortawesome.github.io/Font-Awesome/).
            bundles.Add(new StyleBundle("~/Content/fa", ContentDeliveryNetwork.MaxCdn.FontAwesomeUrl).Include("~/Content/fontawesome/font-awesome.css"));

            // jQuery UI Library
            bundles.Add(new StyleBundle("~/Content/jqueryuicss")
                .Include("~/Content/jqueryUI/jquery-*"));

            // jqGrid Plugin CSS
            bundles.Add(new StyleBundle("~/Content/jqgridcss").Include("~/Content/jqGrid/ui.jqgrid*"));

            // toastr Notification Plugin
            bundles.Add(new StyleBundle("~/Content/toastrcss").Include("~/Content/toastr.min.css"));

            // select2 plugin
            bundles.Add(new StyleBundle("~/Content/select2css")
                .Include("~/Content/select2/select2.min.css")
                .Include("~/Content/select2/select2-bootstrap.min.css"));
        }

        /// <summary>
        /// Creates and adds JavaScript bundles to the bundle collection. Content Delivery Network's (CDN) are used
        /// where available.
        ///
        /// Note: MVC's built in <see cref="System.Web.Optimization.Bundle.CdnFallbackExpression"/> is not used as
        /// using in-line scripts is not permitted under Content Security Policy (CSP) (See <see cref="FilterConfig"/>
        /// for more details).
        ///
        /// Instead, we create our own fail-over bundles. If a CDN is not reachable, the fail-over script loads the
        /// local bundles instead. The fail-over script is only a few lines of code and should have a minimal impact,
        /// although it does add an extra request (Two if the browser is IE8 or less). If you feel confident in the CDN
        /// availability and prefer better performance, you can delete these lines.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        private static void AddJavaScript(BundleCollection bundles)
        {
            // jQuery - The JavaScript helper API (http://jquery.com/).
            Bundle jqueryBundle = new ScriptBundle("~/bundles/jquery", ContentDeliveryNetwork.Google.JQueryUrl)
                .Include("~/Scripts/jquery-{version}.js");
            bundles.Add(jqueryBundle);

            // jQuery UI Library
            Bundle jqueryUI = new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui*");
            bundles.Add(jqueryUI);

            // jQuery Validate - Client side JavaScript form validation (http://jqueryvalidation.org/).
            Bundle jqueryValidateBundle = new ScriptBundle(
                "~/bundles/jqueryval",
                ContentDeliveryNetwork.Microsoft.JQueryValidateUrl)
                .Include("~/Scripts/jquery.validate*");
            bundles.Add(jqueryValidateBundle);

            // Microsoft jQuery Validate Unobtrusive - Validation using HTML data- attributes
            // http://stackoverflow.com/questions/11534910/what-is-jquery-unobtrusive-validation
            Bundle jqueryValidateUnobtrusiveBundle = new ScriptBundle(
                "~/bundles/jqueryvalunobtrusive",
                ContentDeliveryNetwork.Microsoft.JQueryValidateUnobtrusiveUrl)
                .Include("~/Scripts/jquery.validate*");
            bundles.Add(jqueryValidateUnobtrusiveBundle);

            // Modernizr - Allows you to check if a particular API is available in the browser (http://modernizr.com).
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            // Note: The current version of Modernizr does not support Content Security Policy (CSP) (See FilterConfig).
            // See here for details: https://github.com/Modernizr/Modernizr/pull/1263 and
            // http://stackoverflow.com/questions/26532234/modernizr-causes-content-security-policy-csp-violation-errors
            Bundle modernizrBundle = new ScriptBundle(
                "~/bundles/modernizr")
                .Include("~/Scripts/modernizr-*");
            bundles.Add(modernizrBundle);

            // Bootstrap - Twitter Bootstrap JavaScript (http://getbootstrap.com/).
            Bundle bootstrapBundle = new ScriptBundle(
                "~/bundles/bootstrap",
                ContentDeliveryNetwork.Microsoft.BootstrapUrl)
                .Include("~/Scripts/bootstrap.js");
            bundles.Add(bootstrapBundle);

            // Script bundle for the site. The fall-back scripts are for when a CDN fails, in this case we load a local
            // copy of the script instead.
            Bundle failoverCoreBundle = new ScriptBundle("~/bundles/site")
                .Include("~/Scripts/Fallback/styles.js")
                .Include("~/Scripts/Fallback/scripts.js")
                .Include("~/Scripts/site.js")
                .Include("~/Scripts/siigma.gestion-alertas.js")
                .Include("~/Scripts/siigma.bases-gestion.js")
                .Include("~/Scripts/siigma.admin-usuarios.js")
                .Include("~/Scripts/siigma.admin-colas.js");
            bundles.Add(failoverCoreBundle);

            // jqGrid Plugin
            Bundle jqgrid = new ScriptBundle("~/bundles/jqgrid")
                .Include("~/Scripts/jquery.jqGrid.min.js")
                .Include("~/Scripts/i18n/grid.locale-es.js");
            bundles.Add(jqgrid);

            // jQuery Formatters (Date / Number)
            Bundle jqueryFormats = new ScriptBundle("~/bundles/jqueryformats")
                .Include("~/Scripts/jquery.dateformatter.js")
                .Include("~/Scripts/jquery.number.min.js");
            bundles.Add(jqueryFormats);

            // toastr Notification Plugin
            Bundle toastrBundle = new ScriptBundle("~/bundles/toastrjs")
                .Include("~/Scripts/toastr.min.js");
            bundles.Add(toastrBundle);

            // JavaScript Home/Index
            Bundle testBundle = new ScriptBundle("~/bundles/inicio")
                .Include("~/Scripts/siigma.index-home.js");
            bundles.Add(testBundle);

            Bundle configInicio = new ScriptBundle("~/bundles/configinit")
                .Include("~/Scripts/siigma.index-config.js");
            bundles.Add(configInicio);

            Bundle select2js = new ScriptBundle("~/bundles/select2js")
                .Include("~/Scripts/select2.full.min.js")
                .Include("~/Scripts/i18n/es.js");
            bundles.Add(select2js);
        }
    }
}
