﻿using SIIGMA.Models;
using SIIGMA.Reports;
using SIIGMA.Utils;
using SIIGMA.DTO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SIIGMA.Services
{
    public sealed class MonitoreoService : IMonitoreoService
    {
        public InfoClienteReport GetInfoCliente(long documento)
        {
            if (documento <= 0)
                throw new ArgumentNullException("documento");

            using(Context_DBMineria _db = new Context_DBMineria())
            {
                var infoCliente = _db.TblClientesDemo2015.Where(q => q.DOCUMENTO == documento).FirstOrDefault();
                var grupoSeguridad = _db.TblClientesGrupo2015.Where(q => q.DOCUMENTO == documento).Select(f => f.NOMBRE_GRUPO).FirstOrDefault();
                var infoAnterior = _db.TblClientesDemo.Where(q => q.DOCUMENTO == documento).FirstOrDefault();

                if(infoCliente == null || infoAnterior == null)
                {
                    InfoClienteReport modelo = null;
                    return modelo;
                }

                var fechaActModelo = infoAnterior.FECHA_EJEC.ToString();

                InfoClienteReport model = new InfoClienteReport(infoCliente);
                model.GrupoSeguridad = grupoSeguridad;
                model.Tip = string.Format("La información del cliente no ha cambiado desde la fecha {0}/{1}/{2}", fechaActModelo.Substring(6, 2), fechaActModelo.Substring(4, 2), fechaActModelo.Substring(0, 4));

                bool infoDiff = false;
                if (!model.Telefono1.Equals(infoAnterior.TELEFONO1.ToString()) && infoAnterior.TELEFONO1 > 0)
                { model.Telefono1 = string.Format("{0} ({1})*", model.Telefono1, infoAnterior.TELEFONO1.ToString()); infoDiff = true; }

                if (!model.Telefono2.Equals(infoAnterior.TELEFONO2.ToString()) && infoAnterior.TELEFONO2 > 0)
                { model.Telefono2 = string.Format("{0} ({1})*", model.Telefono2, infoAnterior.TELEFONO2.ToString()); infoDiff = true; }

                if (!model.Celular.Equals(infoAnterior.CELULAR2.ToString()) && infoAnterior.CELULAR2 > 0)
                { model.Celular = string.Format("{0} ({1})*", model.Celular, infoAnterior.CELULAR2.ToString()); infoDiff = true; }

                if (!model.CelularAlertas.Equals(infoAnterior.CELULAR_ALYNOT) && !string.IsNullOrEmpty(infoAnterior.CELULAR_ALYNOT))
                { model.CelularAlertas = string.Format("{0} ({1})*", model.CelularAlertas, infoAnterior.CELULAR_ALYNOT); infoDiff = true; }

                if (!model.Direccion.Equals(infoAnterior.DIR_RESIDENCIA_CIF) && !string.IsNullOrEmpty(infoAnterior.DIR_RESIDENCIA_CIF))
                { model.Direccion = string.Format("{0} ({1})*", model.Direccion, infoAnterior.DIR_RESIDENCIA_CIF); infoDiff = true; }

                if (!model.LugarTrabajo.Equals(infoAnterior.LUGAR_TRABAJO) && !string.IsNullOrEmpty(infoAnterior.LUGAR_TRABAJO))
                { model.LugarTrabajo = string.Format("{0} ({1})*", model.LugarTrabajo, infoAnterior.LUGAR_TRABAJO); infoDiff = true; }

                if (!model.DireccionTrabajo.Equals(infoAnterior.DIR_LABORAL_CIF) && !string.IsNullOrEmpty(infoAnterior.DIR_LABORAL_CIF))
                { model.DireccionTrabajo = string.Format("{0} ({1})*", model.DireccionTrabajo, infoAnterior.DIR_LABORAL_CIF); infoDiff = true; }

                if (!model.Correo.Equals(infoAnterior.CORREO_CIF) && !string.IsNullOrEmpty(infoAnterior.CORREO_CIF))
                { model.Correo = string.Format("{0} ({1})*", model.Correo, infoAnterior.CORREO_CIF); infoDiff = true; }

                if (!model.Ciudad.Equals(infoAnterior.CIUDAD) && !string.IsNullOrEmpty(infoAnterior.CIUDAD))
                { model.Ciudad = string.Format("{0} ({1})*", model.Ciudad, infoAnterior.CIUDAD); infoDiff = true; }

                if (!model.Departamento.Equals(infoAnterior.DEPARTAMENTO) && !string.IsNullOrEmpty(infoAnterior.DEPARTAMENTO))
                { model.Departamento = string.Format("{0} ({1})*", model.Departamento, infoAnterior.DEPARTAMENTO); infoDiff = true; }

                if (!model.Region.Equals(infoAnterior.REGION) && !string.IsNullOrEmpty(infoAnterior.REGION))
                { model.Region = string.Format("{0} ({1})*", model.Region, infoAnterior.REGION); infoDiff = true; }

                if (infoDiff)
                    model.Tip = string.Format("* La información entre parentesis es la que tenía a la fecha {0}/{1}/{2}", fechaActModelo.Substring(6, 2), fechaActModelo.Substring(4, 2), fechaActModelo.Substring(0, 4));

                return model;
            }
        }

        public List<VistaAlertaReport> GetTransacciones(out string regla)
        {
            string[] reglasUsuario;
            using(Context_DBSegban _segban = new Context_DBSegban())
            {
                var usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.LastIndexOf('\\') + 1);
                reglasUsuario = (from uq in _segban.TblUserQueues
                                 join rq in _segban.TblReglaQueues on uq.QueueName equals rq.QueueName
                                 where uq.UserName.Equals(usuario)
                                 select rq.NombreRegla).Distinct().ToArray();
            }

            StringBuilder query = new StringBuilder();
            query.Append("SELECT LLAVE, ID, DOCUMENTO, REGLA, HORA ")
                .Append("FROM dbo.CONSOLIDADO_ALERTAS_NUEVO ")
                .Append("WHERE GESTION = 'Sin Gestionar' AND REGLA IN ({0}) AND ID = ")
                .Append("(SELECT DISTINCT FIRST_VALUE(ID) OVER (ORDER BY HORA ASC) ID ")
                .Append("FROM dbo.CONSOLIDADO_ALERTAS_NUEVO WHERE GESTION = 'Sin Gestionar' AND REGLA IN ({0}))");

            var paramNames = reglasUsuario.Select((s, i) => "@tag" + i.ToString()).ToArray();
            List<SqlParameter> paramList = new List<SqlParameter>();
            for(int i = 0; i < reglasUsuario.Count(); ++i)
            {
                SqlParameter param = new SqlParameter("@tag" + i, reglasUsuario[i]);
                paramList.Add(param);
            }

            using(Context_DBMineria _mineria = new Context_DBMineria())
            {
                var alerta = _mineria.Database.SqlQuery<VistaAlertaReport>(string.Format(query.ToString(), string.Join(",", paramNames)), paramList.ToArray()).ToList();
                if(alerta.Any() || alerta.Count > 0)
                {
                    regla = alerta.Select(f => f.REGLA).Distinct().First();
                    return alerta;
                }
            }

            regla = string.Empty;
            return new List<VistaAlertaReport>();
        }

        public async Task CambiarEstadoReglasGAsync(List<string> llaves, string regla)
        {
            if(string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");
            if(!llaves.Any())
                throw new ArgumentNullException("llaves");

            List<SqlParameter> paramsList = new List<SqlParameter>();

            var paramNames = llaves.Select((s, i) => "@tag" + i.ToString()).ToArray();
            for(int i = 0; i < llaves.Count; ++i)
                paramsList.Add(new SqlParameter("@tag" + i, llaves[i]));

            using (Context_DBMineria _mineria = new Context_DBMineria())
            {
                string query = string.Format("UPDATE [REGLAS].[G_{0}] SET ALERTA_USUARIO = @usuario, ALERTA_GESTION = 'En Proceso' WHERE [LLAVE] IN ({1})", regla, string.Join(",", paramNames));
                paramsList.Add(new SqlParameter("@usuario", HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.LastIndexOf('\\') + 1)));

                await _mineria.Database.ExecuteSqlCommandAsync(query, paramsList.ToArray());
            }
        }

        public List<dynamic> GetDetalleAlerta(string llave, string regla)
        {
            if(string.IsNullOrEmpty(llave))
                throw new ArgumentNullException("llave");
            if(string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");

            using (Context_DBMineria _mineria = new Context_DBMineria())
            {
                string query = string.Format("SELECT * FROM [REGLAS].[R_{0}] WHERE [LLAVE] = @llave", regla.ToUpper());
                var detalles = _mineria.CollectionFromSql(query, new Dictionary<string, object> { { "@llave", llave.ToUpper() } }).ToList();
                return detalles;
            }
        }

        public List<Vista_Historico> GetHistorico(long documento)
        {
            if (documento <= 0)
                throw new ArgumentNullException("documento");

            using(Context_DBMineria _mineria = new Context_DBMineria())
            {
                string query = "SELECT * FROM [REGLAS].[VISTA_MONIT] WHERE [DOCUMENTO] = @documento";
                var historicos = _mineria.Database.SqlQuery<Vista_Historico>(query, new SqlParameter("@documento", documento)).ToList();
                return historicos;
            }
        }
        
        public async Task<List<string>> GetColasAsignadasAsync()
        {
            var usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.LastIndexOf("\\") + 1);

            using (Context_DBSegban _db = new Context_DBSegban())
            {
                var res = _db.Database.SqlQuery<string>("SELECT QueueName FROM [GESTION_DEBITO].[SIIGMA_UserQueues] WHERE UserName = @usuario", new SqlParameter("@usuario", usuario)).ToList();
                await Task.Delay(TimeSpan.FromSeconds(1));
                return res;
            }
        }

        public async Task<T> MapearInfoBasesAsync<T>(List<dynamic> detalleAlerta, NameValueCollection requestForm, bool receptor = false) where T : class, new()
        {
            if(detalleAlerta == null || !detalleAlerta.Any())
                throw new ArgumentNullException("detalleAlerta");
            if(requestForm == null || requestForm.Count <= 0)
                throw new ArgumentNullException("requestForm");

            T model = new T();
            List<MapeoBases> listadoCampos = new List<MapeoBases>();
            var tipoModelo = model.GetType().ToString().Substring(model.GetType().ToString().LastIndexOf('.') + 1);
            var usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.LastIndexOf('\\') + 1);
            var regla = requestForm["regla"].ToString();

            using(Context_DBSegban _db = new Context_DBSegban())
                listadoCampos = _db.TblMapeoClases.Where(f => f.Regla.Equals(regla)).ToList();

            if (receptor)
                tipoModelo = string.Format("{0}-{1}", tipoModelo, "Receptor");

            foreach(var campo in listadoCampos.Where(f => f.Modelo.Equals(tipoModelo)))
            {
                PropertyInfo campoDestino = model.GetType().GetProperty(campo.CampoDestino);
                object origenDato = null;
                bool ingresado_PorDefecto = false;

                if(campo.CampoOrigen.Equals("Ingresado"))
                {
                    origenDato = requestForm[campo.CampoDestino];
                    ingresado_PorDefecto = true;
                }
                else if(campo.CampoOrigen.Equals("PorDefecto"))
                {
                    if(campo.ValorPorDefecto.Equals("FechaActual"))
                        origenDato = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    else if(campo.ValorPorDefecto.Equals("UsuarioRed"))
                        origenDato = usuario;
                    else
                        origenDato = campo.ValorPorDefecto;

                    ingresado_PorDefecto = true;
                }
                else
                    foreach(IDictionary<string, object> row in detalleAlerta)
                        foreach(var f in row)
                            if(f.Key.ToString().Equals(campo.CampoOrigen))
                            {
                                origenDato = f.Value;
                                break;
                            }

                if(origenDato == null || string.IsNullOrEmpty(Convert.ToString(origenDato)))
                    origenDato = 0;

                if(Type.GetTypeCode(campoDestino.PropertyType) == TypeCode.DateTime && !ingresado_PorDefecto)
                    origenDato = App.ExtraerFecha(origenDato);

                campoDestino.SetValue(model, Convert.ChangeType(origenDato, Type.GetTypeCode(campoDestino.PropertyType)));
            }
            await Task.Delay(TimeSpan.FromSeconds(1));

            return model;
        }

        public async Task GuardarSegbanAsync<T>(T entity) where T : class
        {
            if(entity == null)
                throw new ArgumentNullException("entity");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                _db.Set<T>().Add(entity);
                await _db.SaveChangesAsync();
            }
        }

        public async Task GuardarInfoMineriaAsync(List<string> llaves, string regla, string gestion, DateTime horagestion, string observaciones)
        {
            if(string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");
            if(!llaves.Any())
                throw new ArgumentNullException("llaves");
            if(string.IsNullOrEmpty(gestion))
                throw new ArgumentNullException("gestion");
            if(horagestion == null)
                throw new ArgumentNullException("horagestion");
            if(string.IsNullOrEmpty(observaciones))
                throw new ArgumentNullException("observaciones");

            List<SqlParameter> paramsList = new List<SqlParameter>();

            var paramNames = llaves.Select((s, i) => "@tag" + i.ToString()).ToArray();
            for(int i = 0; i < llaves.Count; ++i)
                paramsList.Add(new SqlParameter("@tag" + i, llaves[i]));

            string query = string.Format("UPDATE [REGLAS].[G_{0}] SET ALERTA_GESTION = @gestion, ALERTA_USUARIO = @usuario, ALERTA_HORAGESTION = @horagestion, ALERTA_OBSERVACIONES = @observaciones WHERE [LLAVE] IN ({1})", regla, string.Join(",", paramNames));
            paramsList.Add(new SqlParameter("@gestion", gestion));
            paramsList.Add(new SqlParameter("@usuario", HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.LastIndexOf('\\') + 1)));
            paramsList.Add(new SqlParameter("@horagestion", horagestion));
            paramsList.Add(new SqlParameter("@observaciones", observaciones));

            using(Context_DBMineria _mineria = new Context_DBMineria())
            {
                await _mineria.Database.ExecuteSqlCommandAsync(query, paramsList.ToArray());
            }
        }

        public async Task<EstadisticasDTO> Estadisticas()
        {
            EstadisticasDTO model = new EstadisticasDTO();
            string usuario = HttpContext.Current.User.Identity.Name.Substring(HttpContext.Current.User.Identity.Name.LastIndexOf('\\') + 1);
            List<EstadisticasTiempoReport> listadoTiempos;
            StringBuilder query = new StringBuilder("SELECT DISTINCT NombreUsuario, Estado, AVG(Segundos) PromedioSegundos ");
            query.Append("FROM (SELECT NombreUsuario, Estado, DATEDIFF(second,FechaFinal,FechaInicio) Segundos ")
                .Append("FROM GESTION_DEBITO.SIIGMA_TiemposUsuario ")
                .Append("WHERE FechaFinal != '0001-01-01 00:00:00' AND NombreUsuario = @usuario) Tiempos ")
                .Append("GROUP BY NombreUsuario, Estado ");

            using(Context_DBMineria _db = new Context_DBMineria())
            {
                long fechaAyer = long.Parse(DateTime.Now.AddDays(-1).ToString("yyyyMMdd"));
                var gestionadasAyer = (from hist in _db.TblVistaHistorico
                                        where !hist.GESTION.Equals("Sin Gestionar") && !hist.GESTION.Equals("En Proceso") && hist.FECHA == fechaAyer
                                        select hist);
                var gestionadasHoy = (from al in _db.TblConsolidadoAlertas
                                      where !al.GESTION.Equals("Sin Gestionar") && !al.GESTION.Equals("En Proceso")
                                      select al);
                var pendientesGestionar = (from pg in _db.TblConsolidadoAlertas
                                           where pg.GESTION.Equals("Sin Gestionar") || pg.GESTION.Equals("En Proceso")
                                           select pg);
                
                model.AlertasGestionadasHoy = gestionadasHoy.Where(f => f.USUARIO.Equals(usuario)).Count();
                model.AlertasGestionadasAyer = gestionadasAyer.Where(f => f.ALERTA_USUARIO.Equals(usuario)).Count();
                model.PorcentajeGestion = ((model.AlertasGestionadasAyer * 1) / gestionadasAyer.Count());

               var gestionadasXregla = gestionadasHoy.Where(f => f.USUARIO.Equals(usuario))
                    .GroupBy(f => f.REGLA)
                    .Select(f => new EstadisticasAlertasReport
                    {
                        Regla = f.FirstOrDefault().REGLA,
                        Alertas = f.Count()
                    }
                    );

                var gestionadasAyerXregla = gestionadasAyer.Where(f => f.ALERTA_USUARIO.Equals(usuario))
                    .GroupBy(f => f.REGLA)
                    .Select(f => new EstadisticasAlertasReport
                    {
                        Regla = f.FirstOrDefault().REGLA,
                        Alertas = f.Count()
                    });

                var pendientesXregla = pendientesGestionar.GroupBy(f => f.REGLA)
                    .Select(f => new EstadisticasAlertasReport
                    {
                        Regla = f.FirstOrDefault().REGLA,
                        Alertas = f.Count()
                    });

                model.GestionadasPorRegla = gestionadasXregla.OrderByDescending(f => f.Alertas).ToDictionary(f => f.Regla, f => f.Alertas);
                model.GestionadasAyerPorRegla = gestionadasAyerXregla.OrderByDescending(f => f.Alertas).ToDictionary(f => f.Regla, f => f.Alertas);
                model.PendientesGestionar = pendientesXregla.OrderByDescending(f => f.Alertas).ToDictionary(f => f.Regla, f => f.Alertas);
            }

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                await Task.Delay(TimeSpan.FromMilliseconds(1));
                listadoTiempos = _db.Database.SqlQuery<EstadisticasTiempoReport>(query.ToString(), new SqlParameter("@usuario", HttpContext.Current.User.Identity.Name)).ToList();
            }

            foreach(var tiempo in listadoTiempos)
            {
                var ts = TimeSpan.FromSeconds(tiempo.PromedioSegundos);
                if(tiempo.Estado.Equals("DI"))
                    model.TiempoPromedioDisponible = string.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
                if(tiempo.Estado.Equals("ND"))
                    model.TiempoPromedioNoDisponible = string.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
                if(tiempo.Estado.Equals("GE"))
                    model.TiempoPromedioGestion = string.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
            }

            return model;
        }
    }
}