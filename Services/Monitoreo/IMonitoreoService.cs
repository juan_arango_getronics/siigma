﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIIGMA.Models;
using SIIGMA.Reports;
using System.Collections.Specialized;
using SIIGMA.DTO;

namespace SIIGMA.Services
{
    public interface IMonitoreoService
    {
        InfoClienteReport GetInfoCliente(long documento);
        List<VistaAlertaReport> GetTransacciones(out string regla);
        Task CambiarEstadoReglasGAsync(List<string> llaves, string regla);
        List<dynamic> GetDetalleAlerta(string llave, string regla);
        List<Vista_Historico> GetHistorico(long documento);
        Task<List<string>> GetColasAsignadasAsync();
        Task<T> MapearInfoBasesAsync<T>(List<dynamic> detalleAlerta, NameValueCollection requestForm, bool receptor = false) where T : class, new();
        Task GuardarSegbanAsync<T>(T entity) where T : class;
        Task GuardarInfoMineriaAsync(List<string> llaves, string regla, string gestion, DateTime horagestion, string observaciones);
        Task<EstadisticasDTO> Estadisticas();
    }
}
