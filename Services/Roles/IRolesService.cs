﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SIIGMA.DTO;
using SIIGMA.Models;

namespace SIIGMA.Services
{
    public interface IRolesService
    {
        string[] GetRolesFromDatabase(string username);
        List<Role> GetAll();
        Task<List<UserRoles>> GetConcatUserRolesAsync(JqGridDefaultAttrs model);
        Task EliminarUsuario(string usuario);
        Task AgregarUsuario(string usuario, string[] roles);
        bool IsUserInRole(string usuario, string role);
    }
}
