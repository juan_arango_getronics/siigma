﻿using System;
using System.Linq;
using SIIGMA.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using SIIGMA.DTO;
using System.Text;
using System.Data.SqlClient;

namespace SIIGMA.Services
{
    public sealed class RolesService : IRolesService
    {
        public string[] GetRolesFromDatabase(string username)
        {
            if(string.IsNullOrEmpty(username))
                throw new ArgumentNullException("username", string.Format("[ERR-001]: El nombre de usuario no puede ser nulo; Clase:<{0}>, Metodo:<{1}>", typeof(RolesService).Name, nameof(RolesService.GetRolesFromDatabase)));
            else
                using(Context_DBSegban _db = new Context_DBSegban())
                {
                    return _db.TblUserRoles.Where(f => f.UserName.Equals(username)).Select(f => f.RoleName).ToArray();
                }
        }

        public List<Role> GetAll()
        {
            using (Context_DBSegban _db = new Context_DBSegban())
                return _db.TblRoles.ToList();
        }

        public async Task<List<UserRoles>> GetConcatUserRolesAsync(JqGridDefaultAttrs model)
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT DISTINCT UserName, SUBSTRING(( SELECT ',' + ur2.RoleName AS [text()] FROM [BDSEGBAN].[GESTION_DEBITO].[SIIGMA_UserRoles] ur2 WHERE ur1.UserName = ur2.UserName ORDER BY ur1.UserName FOR XML PATH('')), 2, 1000) RoleName FROM [BDSEGBAN].[GESTION_DEBITO].[SIIGMA_UserRoles] ur1 ");

            if(!string.IsNullOrEmpty(model.searchField) && !string.IsNullOrEmpty(model.searchstring))
                query.Append("WHERE UserName = @usuario");
    
            using(Context_DBSegban _db = new Context_DBSegban())
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                if(!string.IsNullOrEmpty(model.searchField) && !string.IsNullOrEmpty(model.searchstring))
                {
                    var usuario = @"BANCOLOMBIA\" + model.searchstring;
                    return _db.Database.SqlQuery<UserRoles>(query.ToString(), new SqlParameter("@usuario", usuario)).ToList();
                }
                else
                    return _db.Database.SqlQuery<UserRoles>(query.ToString()).ToList();
            }
        }

        public async Task EliminarUsuario(string usuario)
        {
            if(string.IsNullOrEmpty(usuario))
                throw new ArgumentNullException("usuario");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var query = "DELETE FROM [GESTION_DEBITO].[SIIGMA_UserRoles] WHERE UserName = @usuario";
                await _db.Database.ExecuteSqlCommandAsync(query, new SqlParameter("@usuario", usuario));
            }
        }

        public async Task AgregarUsuario(string usuario, string[] roles)
        {
            if(string.IsNullOrEmpty(usuario))
                throw new ArgumentNullException("usuario");
            if(roles.Length < 1)
                throw new ArgumentNullException("roles");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                foreach(string rol in roles.ToList())
                {
                    UserRoles item = new UserRoles("BANCOLOMBIA\\" + usuario, rol);
                    _db.TblUserRoles.Add(item);
                }
                await _db.SaveChangesAsync();
            }
        }

        public bool IsUserInRole(string usuario, string roleName)
        {
            if(string.IsNullOrEmpty(usuario))
                throw new ArgumentNullException(usuario);
            if(string.IsNullOrEmpty(roleName))
                throw new ArgumentNullException(roleName);

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var res = _db.TblUserRoles.Where(f => f.UserName.Equals(usuario) && f.RoleName.Equals(roleName)).FirstOrDefault();
                if(res != null)
                    return true;
                else
                    return false;
            }
        }
    }
}