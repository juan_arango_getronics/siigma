﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SIIGMA.Models;
using SIIGMA.Reports;

namespace SIIGMA.Services
{
    public interface IConfigService
    {
        Task<List<Queues>> GetColas();
        Task AgregarCola(string cola, string descripcion);
        Task EliminarCola(string cola);
        Task AgregarReglasCola(string cola, string regla);
        Task AgregarUsuarioCola(string cola, string usuario);
        Task EliminarReglasCola(string cola, string regla);
        Task EliminarUsuarioCola(string cola, string usuario);
        Task<List<ColaReglasUsuarioReport>> GetColasReglasUsuarios();
        Task<List<string>> GetReglas();
        Task<List<string>> GetUsuarios();
    }
}
