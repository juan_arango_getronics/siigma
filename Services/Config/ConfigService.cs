﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using SIIGMA.Models;
using SIIGMA.Reports;

namespace SIIGMA.Services
{
    public sealed class ConfigService : IConfigService
    {
        public async Task<List<Queues>> GetColas()
        {
            using (Context_DBSegban _db = new Context_DBSegban())
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                return _db.TblQueues.ToList();
            }
        }

        public async Task AgregarCola(string cola, string descripcion)
        {
            if(string.IsNullOrEmpty(cola))
                throw new ArgumentNullException("cola");
            if(string.IsNullOrEmpty(descripcion))
                throw new ArgumentNullException("descripcion");

            using (Context_DBSegban _db = new Context_DBSegban())
            {
                Queues model = new Queues(cola, descripcion);
                _db.TblQueues.Add(model);
                await _db.SaveChangesAsync();
            }
        }

        public async Task EliminarCola(string cola)
        {
            if(string.IsNullOrEmpty(cola))
                throw new ArgumentNullException("cola");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var userQueues = _db.TblUserQueues.Where(f => f.QueueName.Equals(cola)).AsEnumerable();
                var reglasQueues = _db.TblReglaQueues.Where(f => f.QueueName.Equals(cola)).AsEnumerable();
                var queue = _db.TblQueues.Where(f => f.QueueName.Equals(cola)).FirstOrDefault();

                _db.TblUserQueues.RemoveRange(userQueues);
                await _db.SaveChangesAsync();

                _db.TblReglaQueues.RemoveRange(reglasQueues);
                await _db.SaveChangesAsync();

                _db.TblQueues.Remove(queue);
                await _db.SaveChangesAsync();
            }
        }

        public async Task AgregarReglasCola(string cola, string regla)
        {
            if(string.IsNullOrEmpty(cola))
                throw new ArgumentNullException("cola");
            if(string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var aEliminar = _db.TblReglaQueues.Where(f => f.NombreRegla.Equals(regla));

                _db.TblReglaQueues.RemoveRange(aEliminar);
                await _db.SaveChangesAsync();
            }
            using(Context_DBSegban _db = new Context_DBSegban())
            {
                ReglaQueues item = new ReglaQueues(regla, cola);
                _db.TblReglaQueues.Add(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task AgregarUsuarioCola(string cola, string usuario)
        {
            if(string.IsNullOrEmpty(cola))
                throw new ArgumentNullException("cola");
            if(string.IsNullOrEmpty(usuario))
                throw new ArgumentNullException("usuario");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                UserQueues item = new UserQueues(usuario, cola);
                _db.TblUserQueues.Add(item);
                await _db.SaveChangesAsync();
            }
        }

        public async Task EliminarReglasCola(string cola, string regla)
        {
            if(string.IsNullOrEmpty(cola))
                throw new ArgumentNullException("cola");
            if(string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var aEliminar = _db.TblReglaQueues.Where(f => f.NombreRegla.Equals(regla));

                _db.TblReglaQueues.RemoveRange(aEliminar);
                await _db.SaveChangesAsync();
            }
        }

        public async Task EliminarUsuarioCola(string cola, string usuario)
        {
            if(string.IsNullOrEmpty(cola))
                throw new ArgumentNullException("cola");
            if(string.IsNullOrEmpty(usuario))
                throw new ArgumentNullException("regla");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var aEliminar = _db.TblUserQueues.Where(f => f.UserName.Equals(usuario));

                _db.TblUserQueues.RemoveRange(aEliminar);
                await _db.SaveChangesAsync();
            }
        }

        public async Task<List<ColaReglasUsuarioReport>> GetColasReglasUsuarios()
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT DISTINCT Q.QueueName Cola, ")
                .Append("SUBSTRING((SELECT ', ' + RQ.NombreRegla AS [text()] FROM [BDSEGBAN].[GESTION_DEBITO].[SIIGMA_ReglaQueues] RQ WHERE RQ.QueueName = Q.QueueName ORDER BY RQ.QueueName FOR XML PATH('')), 2, 1000) Reglas, ")
                .Append("SUBSTRING((SELECT ', ' + UQ.UserName AS [text()] FROM [BDSEGBAN].[GESTION_DEBITO].[SIIGMA_UserQueues] UQ WHERE UQ.QueueName = Q.QueueName ORDER BY UQ.QueueName FOR XML PATH('')), 2, 1000) Usuarios ")
                .Append("FROM [GESTION_DEBITO].[SIIGMA_Queues] Q ")
                .Append("LEFT JOIN [GESTION_DEBITO].[SIIGMA_ReglaQueues] RQ ON RQ.QueueName = Q.QueueName ")
                .Append("LEFT JOIN [GESTION_DEBITO].[SIIGMA_UserQueues] UQ ON UQ.QueueName = Q.QueueName ")
                .Append("ORDER BY Cola");

            using (Context_DBSegban _db = new Context_DBSegban())
            {
                var res = _db.Database.SqlQuery<ColaReglasUsuarioReport>(query.ToString()).ToList();
                await Task.Delay(TimeSpan.FromMilliseconds(1));

                return res;
            }
        }

        public async Task<List<string>> GetReglas()
        {
            using (Context_DBSegban _db = new Context_DBSegban())
            {
                var res = _db.TblReglas.Select(f => f.Regla).Distinct().ToList();
                await Task.Delay(TimeSpan.FromMilliseconds(1));

                return res;
            }
        }

        public async Task<List<string>> GetUsuarios()
        {
            using(Context_DBSegban _db = new Context_DBSegban())
            {
                var res = _db.TblUserRoles.Where(f => f.RoleName.Equals("Monitoreo")).Select(f => f.UserName).Distinct().ToList();
                await Task.Delay(TimeSpan.FromMilliseconds(1));

                return res;
            }
        }
    }
}