﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIIGMA.Models;

namespace SIIGMA.Services
{
    public sealed class ReglasService : IReglasService
    {
        public bool AlmacenaBases(string regla)
        {
            if (string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");

            using (Context_DBSegban _db = new Context_DBSegban())
            {
                bool almacenaBases = _db.TblReglas.Where(f => f.Regla.Equals(regla)).Select(f => f.AlmacenaBases).FirstOrDefault();
                return almacenaBases;
            }
        }

        public bool RegistraReceptor(string regla)
        {
            if (string.IsNullOrEmpty(regla))
                throw new ArgumentNullException("regla");

            using(Context_DBSegban _db = new Context_DBSegban())
            {
                bool registraReceptor = _db.TblReglas.Where(f => f.Regla.Equals(regla)).Select(f => f.RegistraReceptor).FirstOrDefault();
                return registraReceptor;
            }
        }
    }
}