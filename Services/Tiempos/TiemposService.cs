﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SIIGMA.Models;

namespace SIIGMA.Services
{
    public sealed class TiemposService : ITiemposService
    {
        public async Task CambiarEstado(string estado)
        {
            if(string.IsNullOrEmpty(estado))
                throw new ArgumentNullException("estado");

            var usuario = HttpContext.Current.User.Identity.Name;
            using (Context_DBSegban _db = new Context_DBSegban())
            {
                if(HttpContext.Current.Session["UserTime"] != null)
                {
                    var model = (TiemposUsuario)HttpContext.Current.Session["UserTime"];
                    model.FechaFinal = DateTime.Now;
                    _db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                }

                TiemposUsuario registro = new TiemposUsuario(Guid.NewGuid().ToString(), usuario, estado, DateTime.Now.ToUniversalTime());
                _db.TblTiemposUsuario.Add(registro);

                await _db.SaveChangesAsync();

                HttpContext.Current.Session["UserTime"] = registro;
            }
        }
    }
}