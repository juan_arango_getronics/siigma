﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIIGMA.Services
{
    public interface ITiemposService
    {
        Task CambiarEstado(string estado);
    }
}
