﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIIGMA.Models;

namespace SIIGMA.Reports
{
    public class InfoClienteReport
    {
        public InfoClienteReport(Clientes_Demo_2015 model)
        {
            this.Documento = model.DOCUMENTO.ToString();
            this.TipoDocumento = model.TIPODOC == null ? "" : model.TIPODOC.ToString();
            this.Nombre = model.NOMBRE == null ? "" : model.NOMBRE.ToString();
            this.FechaVinculacion = model.FECHA_VIN.ToString();
            this.Edad = model.EDAD.ToString();
            this.Profesion = model.NOMBRE_PROFESION == null ? "" : model.NOMBRE_PROFESION.ToString();
            this.Ocupacion = model.NOM_OCUPACION == null ? "" : model.NOM_OCUPACION.ToString();
            this.FechaActDatos = model.FECHAACT.ToString();
            this.Segmento = model.SEGMENTO == null ? "" : model.SEGMENTO.ToString();
            this.FechaActInformacion = model.FECHA_EJEC.ToString();
            this.Telefono1 = model.TELEFONO1.ToString();
            this.Telefono2 = model.TELEFONO2.ToString();
            this.Celular = model.CELULAR2.ToString();
            this.CelularAlertas = model.CELULAR_ALYNOT == null ? "" : model.CELULAR_ALYNOT.ToString();
            this.Direccion = model.DIR_RESIDENCIA_CIF == null ? "" : model.DIR_RESIDENCIA_CIF.ToString();
            this.LugarTrabajo = model.LUGAR_TRABAJO == null ? "" : model.LUGAR_TRABAJO.ToString();
            this.DireccionTrabajo = model.DIR_LABORAL_CIF == null ? "" : model.DIR_LABORAL_CIF.ToString();
            this.Correo = model.CORREO_CIF == null ? "" : model.CORREO_CIF.ToString();
            this.Ciudad = model.CIUDAD == null ? "" : model.CIUDAD.ToString();
            this.Departamento = model.DEPARTAMENTO == null ? "" : model.DEPARTAMENTO.ToString();
            this.Region = model.REGION == null ? "" : model.REGION.ToString();
        }
        public InfoClienteReport() { }

        #region Helpers
        private string _fechaVinculacion;
        private string _fechaActDatos;
        private string _fechaActInformacion;
        #endregion

        public string Documento { get; set; }
        public string TipoDocumento { get; set; }
        public string Nombre { get; set; }
        public string FechaVinculacion
        {
            get
            {
                return string.IsNullOrEmpty(_fechaVinculacion) ? "0" : string.Format("{0}/{1}/{2}", _fechaVinculacion.Substring(6, 2), _fechaVinculacion.Substring(4, 2), _fechaVinculacion.Substring(0, 4));
            }
            set
            {
                _fechaVinculacion = value;
            }
        }
        public string Edad { get; set; }
        public string Profesion { get; set; }
        public string Ocupacion { get; set; }
        public string FechaActDatos
        {
            get
            {
                return string.IsNullOrEmpty(_fechaActDatos) ? "0" : string.Format("{0}/{1}/{2}", _fechaActDatos.Substring(6, 2), _fechaActDatos.Substring(4, 2), _fechaActDatos.Substring(0, 4));
            }
            set
            {
                _fechaActDatos = value;
            }
        }
        public string Segmento { get; set; }
        public string GrupoSeguridad { get; set; }
        public string FechaActInformacion
        {
            get
            {
                return string.IsNullOrEmpty(_fechaActInformacion) ? "0" : string.Format("{0}/{1}/{2}", _fechaActInformacion.Substring(6, 2), _fechaActInformacion.Substring(4, 2), _fechaActInformacion.Substring(0, 4));
            }
            set
            {
                _fechaActInformacion = value;
            }
        }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Celular { get; set; }
        public string CelularAlertas { get; set; }
        public string Direccion { get; set; }
        public string LugarTrabajo { get; set; }
        public string DireccionTrabajo { get; set; }
        public string Correo { get; set; }
        public string Ciudad { get; set; }
        public string Departamento { get; set; }
        public string Tip { get; set; }
        public string Region { get; set; }
    }
}