﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Reports
{
    public class VistaAlertaReport
    {
        public string LLAVE { get; set; }
        public string ID { get; set; }
        public string DOCUMENTO { get; set; }
        public string REGLA { get; set; }
        public DateTime HORA { get; set; }
    }
}