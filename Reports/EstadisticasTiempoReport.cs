﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Reports
{
    public class EstadisticasTiempoReport
    {
        public string NombreUsuario { get; set; }
        public string Estado { get; set; }
        public int PromedioSegundos { get; set; }
    }
}