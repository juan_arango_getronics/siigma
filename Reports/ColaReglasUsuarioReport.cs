﻿namespace SIIGMA.Reports
{
    public class ColaReglasUsuarioReport
    {
        public string Cola { get; set; }
        public string Reglas { get; set; }
        public string Usuarios { get; set; }
    }
}