﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Reports
{
    public class EstadisticasAlertasReport
    {
        public string Regla { get; set; }
        public int Alertas { get; set; }
    }
}