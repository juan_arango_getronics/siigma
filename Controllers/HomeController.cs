﻿namespace SIIGMA.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Constants;
    using DTO;
    using Services;

    public class HomeController : Controller
    {
        #region Fields

        private readonly ILoggingService _logginService;
        private readonly IMonitoreoService _monitoreoService;
        private readonly IRolesService _roleService;

        #endregion

        #region Constructors

        public HomeController(
            ILoggingService logginService,
            IMonitoreoService monitoreoService,
            IRolesService roleService)
        {
            this._logginService = logginService;
            this._monitoreoService = monitoreoService;
            this._roleService = roleService;
        }

        #endregion

        [Route("", Name = HomeControllerRoute.GetIndex)]
        public async Task<ActionResult> Index()
        {
            EstadisticasDTO model = new EstadisticasDTO();

            try
            {
                model = await _monitoreoService.Estadisticas();
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return this.View(HomeControllerAction.Index, model);
        }

        /*private List<string> GetUserList()
        {
            List<string> activeSessions = new List<string>();
            object obj = typeof(HttpRuntime).GetProperty("CacheInternal", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static).GetValue(null, null);
            object[] obj2 =  
        }*/
    }
}