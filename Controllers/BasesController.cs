﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIIGMA.Models;
using SIIGMA.Constants;
using SIIGMA.DTO;
using SIIGMA.Services;
using System.Linq.Dynamic;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.ComponentModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace SIIGMA.Controllers
{
    [RoutePrefix("bases")]
    public class BasesController : Controller
    {
        #region Fields
        private readonly ILoggingService _logginService;
        #endregion

        #region Constructors
        public BasesController(ILoggingService logginService) {
            this._logginService = logginService;
        }
        #endregion

        [Route("listarproductos", Name = BasesControllerRoute.ListarProductos)]
        public JsonResult ListarProductos([Bind(Include = "sidx,sord,page,rows,documento,searchField,searchString,searchOper")]ListarProductosDTO model)
        {

            if(model == null || !ModelState.IsValid)
                return Json(new { total = 0, model.page, records = 0, rows = "" }, JsonRequestBehavior.AllowGet);
            if(string.IsNullOrEmpty(model.documento))
                return Json(new { total = 0, model.page, records = 0, rows = "" }, JsonRequestBehavior.AllowGet);

            int index = model.page - 1;
            int size = model.rows;

            Context_DBSegban _db = new Context_DBSegban();
            if(string.IsNullOrEmpty(model.searchField) || string.IsNullOrEmpty(model.searchstring))
            {
                var list = _db.TblInfoProducto.Where(q => q.ID_CLIENTE.Equals(model.documento));
                int totalreg = list.Count();
                int pages = (int)Math.Ceiling((float) totalreg / (float) model.rows);
                list = list.OrderBy(string.Format("{0} {1}", model.sidx, model.sord));

                list = list.Skip(index * size).Take(size);
                return Json(new { total = pages, model.page, records = totalreg, rows = list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StringBuilder query = new StringBuilder("SELECT * FROM [GESTION_DEBITO].[Tbl_Informacion_Producto] WHERE ID_CLIENTE = @p0").Append(construirCondicion(model.searchField, model.searchOper));
                IQueryable<InfoProducto> list = new List<InfoProducto>().AsQueryable();
                try
                {
                    list = _db.Database.SqlQuery<InfoProducto>(query.ToString(), model.documento, model.searchstring).AsQueryable();
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                int totalreg = list.Count();
                int pages = (int) System.Math.Ceiling((float) totalreg / (float) model.rows);
                list = list.OrderBy(string.Format("{0} {1}", model.sidx, model.sord));

                list = list.Skip(index * size).Take(size);
                return Json(new { total = pages, model.page, records = totalreg, rows = list }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("jqgriddispatcher", Name = BasesControllerRoute.JqGridDispatcher)]
        public JsonResult JqGridDataDispatcher([Bind(Include = "sender,sidx,sord,page,rows,documento,producto,prod_numero,cuenta_numero,searchField,searchString,searchOper")]JqGridDataDispatcherDTO model)
        {
            model.searchField = string.Empty;
            model.searchOper = string.Empty;
            model.searchstring = string.Empty;

            int index = model.page - 1;
            int size = model.rows;

            IQueryable<dynamic> list = Enumerable.Empty<dynamic>().AsQueryable();
            Context_DBSegban _db = new Context_DBSegban();
            switch(model.sender)
            {
                case ".gr-trx":
                    list = _db.TblInfoTransaccion.Where(q => q.ID_CLIENTE.Equals(model.documento) &&
                        q.TIPO_PRODUCTO.Equals(model.producto) && q.NUMERO_PRODUCTO.Equals(model.prod_numero) && q.NUMERO_CUENTA.Equals(model.cuenta_numero));
                    break;
                case ".gr-novedad":
                    list = _db.TblNovedadTransaccion.Where(q => q.ID_CLIENTE.Equals(model.documento) &&
                        q.TIPO_PRODUCTO.Equals(model.producto) && q.NUMERO_PRODUCTO.Equals(model.prod_numero) && q.NUMERO_CUENTA.Equals(model.cuenta_numero));
                    break;
                case ".gr-fraude":
                    list = _db.TblMarcacionFraude.Where(q => q.ID_CLIENTE.Equals(model.documento) &&
                        q.TIPO_PRODUCTO.Equals(model.producto) && q.NUMERO_PRODUCTO.Equals(model.prod_numero) && q.NUMERO_CUENTA.Equals(model.cuenta_numero));
                    break;
                default:
                    break;
            }

            int totalreg = list.Count();
            int pages = (int) Math.Ceiling(totalreg / (float) model.rows);
            list = list.OrderBy(string.Format("{0} {1}", model.sidx, model.sord));
            list = list.Skip(index * size).Take(size);

            return Json(new { total = pages, model.page, records = totalreg, rows = list }, JsonRequestBehavior.AllowGet);
        }

        [Route("_makedropdown", Name = BasesControllerRoute.MakeDropdown)]
        public async Task<ActionResult> _MakeDropdown(string t)
        {
            if(string.IsNullOrEmpty(t))
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            Dictionary<string, string> list = new Dictionary<string, string>();
            Context_DBSegban _db = new Context_DBSegban();
            switch(t.ToLower())
            {
                case "t_bloqueo":
                    var bloqs = _db.TblTipoBloqueo;
                    foreach(var b in bloqs)
                        list.Add(b.GESTION, b.GESTION);
                    break;
                case "t_prod":
                    var prods = _db.TblProducto;
                    foreach(var p in prods)
                        list.Add(p.TIPO_PRODUCTO, p.TIPO_PRODUCTO);
                    break;
                case "t_estadotxa":
                    var est = _db.TblEstadoTransaccion;
                    foreach(var e in est)
                        list.Add(e.MARCACION_TXA, e.MARCACION_TXA);
                    break;
                case "t_canaltxa":
                    var canales = _db.TblCanal;
                    foreach(var c in canales)
                        list.Add(c.CANAL, c.NOMBRE_CANAL);
                    break;
                case "t_tipotxa":
                    var tipos = _db.TblTipoTransaccion;
                    foreach(var tp in tipos)
                        list.Add(tp.TIPO_TRANSACCION, tp.TIPO_TRANSACCION);
                    break;
                case "t_ciudadtxa":
                    var ciudades = _db.TblCiudad.OrderBy("depto ASC, ciudad ASC");
                    foreach(var cd in ciudades)
                        list.Add(cd.codCiudad.ToString(), string.Format("{0}, {1}", cd.ciudad, cd.depto));
                    break;
                case "t_paistxa":
                    var paises = _db.TblPais;
                    foreach(var pa in paises)
                        list.Add(pa.Codigo_Pais, string.Format("{0}, {1}", pa.Codigo_Pais, pa.NPais));
                    break;
                case "t_resptxa":
                    var resp = _db.TblRespuestaTxa;
                    foreach(var re in resp)
                        list.Add(re.COD_RESPUESTA, string.Format("{0}, {1}", re.COD_RESPUESTA, re.DESCRIPCION_RESPUESTA));
                    break;
                case "t_riesgotxa":
                    var riesgos = _db.TblRiesgoAlerta;
                    foreach(var ri in riesgos)
                        list.Add(ri.RIESGO_ALERTA, ri.RIESGO_ALERTA);
                    break;
                case "t_herrtxa":
                    var herrs = _db.TblHerramientaDeteccion;
                    foreach(var h in herrs)
                        list.Add(h.HERRAMIENTA_DETECCION, h.HERRAMIENTA_DETECCION);
                    break;
                case "t_causadesm":
                    var caus = _db.TblCausasDesmarcacion;
                    foreach(var ca in caus)
                        list.Add(ca.CAUSA_DESMARCACION, ca.CAUSA_DESMARCACION);
                    break;
                case "t_empleado":
                    var emps = _db.TblEmpleado;
                    foreach(var e in emps)
                        list.Add(e.CEDULA, string.Format("{0}, {1}", e.CEDULA, e.NOMBRE));
                    break;
                case "t_oficina":
                    var ofs = _db.TblOficina.OrderBy("DEPARTAMENTO ASC, NOMBRE_OFICINA ASC");
                    foreach(var of in ofs)
                        list.Add(of.CODIGO, string.Format("[{0}] {1}, {2}", of.CODIGO, of.NOMBRE_OFICINA, of.DEPARTAMENTO));
                    break;
                case "t_usuario":
                    var usus = _db.TblUsuario.OrderBy(q => q.USUARIO_BASE);
                    foreach(var u in usus)
                        list.Add(u.USUARIO_RED, string.Format("[{0}], {1} {2}", u.USUARIO_RED, u.NOMBRE, u.APELLIDO));
                    break;
                case "t_modalidad":
                    var mods = _db.TblModalidad;
                    foreach(var mod in mods)
                        list.Add(mod.MODALIDAD, mod.MODALIDAD);
                    break;
                default:
                    break;
            }

            await Task.Delay(TimeSpan.FromSeconds(1));
            ViewBag.Items = list;
            return PartialView();
        }

        [ValidateAntiForgeryToken]
        [Route("guardarbloqueo", Name = BasesControllerRoute.GuardarBloqueo)]
        public async Task<ActionResult> GuardarBloqueo([Bind(Exclude = "Id")]InfoProducto model, string oper, string id)
        {
            if(model == null || string.IsNullOrEmpty(oper))
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            if(ModelState.IsValid)
            {
                Context_DBSegban _db = new Context_DBSegban();
                if(!string.IsNullOrEmpty(id) && oper.Equals("edit"))
                    _db.Entry(model).State = EntityState.Modified;
                else if(id.Equals("_empty") && oper.Equals("add"))
                    _db.TblInfoProducto.Add(model);
                else
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.Ambiguous);

                var res = 1;
                try
                {
                    res = await _db.SaveChangesAsync();
                }
                catch(Exception e) when(e is DbUpdateException || e is DbUpdateConcurrencyException || e is DbEntityValidationException || e is NotSupportedException || e is InvalidOperationException)
                {
                    _logginService.Log(e);
                }
                if(res >= 1)
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [Route("guardartrx", Name = BasesControllerRoute.GuardarTrx)]
        public async Task<ActionResult> GuardarTrx([Bind(Exclude = "Id")]InfoTransaccion model, string oper, string id)
        {
            if(model == null || string.IsNullOrEmpty(oper))
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            if(ModelState.IsValid)
            {
                Context_DBSegban _db = new Context_DBSegban();
                if(!string.IsNullOrEmpty(id) && oper.Equals("edit"))
                {
                    model.Id = int.Parse(id);
                    _db.Entry(model).State = EntityState.Modified;
                }
                else if(id.Equals("_empty") && oper.Equals("add"))
                    _db.TblInfoTransaccion.Add(model);
                else
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.Ambiguous);

                int res = 1;
                try
                {
                    res = await _db.SaveChangesAsync();
                }
                catch(Exception e) when(e is DbUpdateException || e is DbUpdateConcurrencyException || e is DbEntityValidationException || e is NotSupportedException || e is InvalidOperationException)
                {
                    _logginService.Log(e);
                }
                if(res >= 1)
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [Route("guardarnovedad", Name = BasesControllerRoute.GuardarNovedad)]
        public async Task<ActionResult> GuardarNovedad([Bind(Exclude = "Id")]NovedadTransaccion model, string oper, string id)
        {
            if(model == null || string.IsNullOrEmpty(oper))
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            if(ModelState.IsValid)
            {
                Context_DBSegban _db = new Context_DBSegban();
                if(!string.IsNullOrEmpty(id) && oper.Equals("edit"))
                    _db.Entry(model).State = EntityState.Modified;
                else if(id.Equals("_empty") && oper.Equals("add"))
                    _db.TblNovedadTransaccion.Add(model);
                else
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.Ambiguous);

                var res = 1;
                try
                {
                    res = await _db.SaveChangesAsync();
                }
                catch(Exception e) when(e is DbUpdateException || e is DbUpdateConcurrencyException || e is DbEntityValidationException || e is NotSupportedException || e is InvalidOperationException)
                {
                    _logginService.Log(e);
                }
                if(res >= 1)
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [Route("guardarfraude", Name = BasesControllerRoute.GuardarFraude)]
        public async Task<ActionResult> GuardarFraude([Bind(Exclude = "Id")]MarcacionFraude model, string oper, string id)
        {
            if(model == null || string.IsNullOrEmpty(oper))
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

            if(ModelState.IsValid)
            {
                Context_DBSegban _db = new Context_DBSegban();
                if(!string.IsNullOrEmpty(id) && oper.Equals("edit"))
                    _db.Entry(model).State = EntityState.Modified;
                else if(id.Equals("_empty") && oper.Equals("add"))
                    _db.TblMarcacionFraude.Add(model);
                else
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.Ambiguous);

                var res = 1;
                try
                {
                    res = await _db.SaveChangesAsync();
                }
                catch(Exception e) when(e is DbUpdateException || e is DbUpdateConcurrencyException || e is DbEntityValidationException || e is NotSupportedException || e is InvalidOperationException)
                {
                    _logginService.Log(e);
                }
                if(res >= 1)
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Route("exportarexcel", Name = BasesControllerRoute.ExportarExcel)]
        public ActionResult ExportarExcel()
        {
            Context_DBSegban _db = new Context_DBSegban();
            StringBuilder query = new StringBuilder();
            var list = _db.TblMarcacionFraude.ToList();

            WriteExcel(ConvertToDataTable(list), "xlsx");
            return new EmptyResult();
        }

        [HttpPost]
        [Route("ingresomasivo", Name = BasesControllerRoute.IngresoMasivo)]
        public async Task<ActionResult> IngresoMasivo(FormCollection formCollection)
        {
            if(Request != null)
            {
                string id_Cliente = Request.Params["hidID_CLIENTE"].ToString();
                string tipo_Producto = Request.Params["hidTIPO_PRODUCTO"].ToString();
                string numero_Producto = Request.Params["hidNUMERO_PRODUCTO"].ToString();
                string numero_Cuenta = Request.Params["hidNUMERO_CUENTA"].ToString();

                if(string.IsNullOrEmpty(id_Cliente) || string.IsNullOrEmpty(tipo_Producto) || string.IsNullOrEmpty(numero_Producto) || string.IsNullOrEmpty(numero_Cuenta))
                    return Json(new { success = false, message = "Debe seleccionar un registro de productos para insertar las transacciones." }, JsonRequestBehavior.AllowGet);

                HttpPostedFileBase file = Request.Files["MasivoTrx"];
                if((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string extension = Path.GetExtension(file.FileName).ToLower();
                    string query = string.Empty;
                    string connString = string.Empty;
                    string[] validFileTypes = { ".xls", ".xlsx", ".csv" };
                    string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/Uploads"), file.FileName);
                    DataTable dataFile = new DataTable();

                    if(!Directory.Exists(path1))
                        Directory.CreateDirectory(Server.MapPath("~/Content/Uploads"));
                    if(validFileTypes.Contains(extension))
                    {
                        if(System.IO.File.Exists(path1))
                            System.IO.File.Delete(path1);
                        file.SaveAs(path1);

                        if(extension.Trim().Equals(".csv"))
                            dataFile = ConvertCSVtoDataTable(path1);
                        else if(extension.Trim().Equals(".xls"))
                        {
                            connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\"", path1);
                            dataFile = ConvertXSLXtoDataTable(path1, connString);
                        }
                        else if(extension.Trim().Equals(".xlsx"))
                        {
                            connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"", path1);
                            dataFile = ConvertXSLXtoDataTable(path1, connString);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, message = "Tipo de archivo NO valido" }, JsonRequestBehavior.AllowGet);
                    }

                    var dataList = dataFile.AsEnumerable();
                    foreach(DataRow dr in dataList)
                    {
                        InfoTransaccion trx = new InfoTransaccion();
                        trx.ID_CLIENTE = id_Cliente;
                        trx.TIPO_PRODUCTO = tipo_Producto;
                        trx.NUMERO_PRODUCTO = numero_Producto;
                        trx.NUMERO_CUENTA = numero_Cuenta;
                        string fechaTrx = dr[0].ToString().Trim();
                        trx.FECHA_TXA = DateTime.Parse(string.Format("{0}/{1}/{2}", fechaTrx.Substring(6, 2), fechaTrx.Substring(4, 2), fechaTrx.Substring(0, 4)));
                        string horaTrx = dr[1].ToString().Trim();
                        trx.HORA_TXA = horaTrx.Length == 5 ? DateTime.Parse(string.Format("1899-12-30 0{0}:{1}:{2}", horaTrx.Substring(0, 1), horaTrx.Substring(2, 2), horaTrx.Substring(3, 2))) : DateTime.Parse(string.Format("1899-12-30 {0}:{1}:{2}", horaTrx.Substring(0, 2), horaTrx.Substring(2, 2), horaTrx.Substring(4, 2)));
                        trx.VALOR_TXA = decimal.Parse(dr[2].ToString().Trim());
                        trx.CANAL_TXA = dr[3].ToString().Trim();
                        trx.NOMBRE_DISPOSITIVO = dr[4].ToString().Trim();
                        trx.TIPO_TXA = dr[5].ToString().Trim();
                        trx.CIUDAD_TXA = dr[6].ToString().Trim();
                        trx.PAIS_TXA = dr[7].ToString().Trim();
                        trx.RESPUESTA_TXA = dr[8].ToString().Trim();
                        trx.CUENTA_RECEPTORA = dr[9].ToString().Trim();
                        trx.IP_TXA = dr[10].ToString().Trim();
                        trx.RIESGO_ALERTA = dr[11].ToString().Trim();
                        trx.MODO_DETECCION = dr[12].ToString().Trim();
                        trx.ESTADO_TXA = dr[13].ToString().Trim();
                        trx.FECHA_INGRESO = DateTime.Now;
                        trx.HORA_INGRESO = DateTime.Now;
                        trx.USUARIO_INGRESO = User.Identity.Name.Substring(User.Identity.Name.LastIndexOf('\\') + 1);

                        using(Context_DBSegban _db = new Context_DBSegban())
                        {
                            _db.TblInfoTransaccion.Add(trx);
                            await _db.SaveChangesAsync();
                        }
                    }
                }
                else
                {
                    return Json(new { success = false, message = "The file is empty" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, message = "The request is null" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        #region Helpers
        private void WriteExcel(DataTable dt, string ext)
        {
            IWorkbook wb;
            if(ext.Equals("xlsx"))
                wb = new XSSFWorkbook();
            else if(ext.Equals("xls"))
                wb = new HSSFWorkbook();
            else
                throw new System.Exception("Formato no permitido");

            ISheet sh = wb.CreateSheet("Informe_Fraude");
            IRow fila = sh.CreateRow(0);
            for(int i = 0; i < dt.Columns.Count; i++)
            {
                ICell celda = fila.CreateCell(i);
                string nombre = dt.Columns[i].ToString();
                celda.SetCellValue(nombre);
            }
            for(int x = 0; x < dt.Rows.Count; x++)
            {
                IRow f = sh.CreateRow(x + 1);
                for(int y = 0; y < dt.Columns.Count; y++)
                {
                    ICell celda = f.CreateCell(y);
                    string indColumna = dt.Columns[y].ToString();
                    celda.SetCellValue(dt.Rows[x][indColumna].ToString());
                }
            }

            using(var datosExp = new MemoryStream())
            {
                Response.Clear();
                wb.Write(datosExp);
                if(ext.Equals("xlsx"))
                {
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("Content-Disposition", "attachment;filename=info-fraudes.xlsx");
                    Response.BinaryWrite(datosExp.ToArray());
                }
                else if(ext.Equals("xls"))
                {
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=info-fraudes.xlsx");
                    Response.BinaryWrite(datosExp.GetBuffer());
                }
                Response.End();
            }
        }
        private DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach(PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach(T item in data)
            {
                DataRow row = table.NewRow();
                foreach(PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        private string construirCondicion(string campo, string operador)
        {
            if(string.IsNullOrEmpty(campo))
                throw new ArgumentNullException("campo");
            if(string.IsNullOrEmpty(operador))
                throw new ArgumentNullException("operador");

            switch(operador.ToLower())
            {
                case "eq":
                    return string.Format(" AND {0} = @p1 ", campo);
                case "ne":
                    return string.Format(" AND {0} != @p1 ", campo);
                default:
                    return string.Format(" AND {0} = @p1 ", campo);
            }
        }
        private static DataTable ConvertCSVtoDataTable(string filePath)
        {
            DataTable dt = new DataTable();
            using(StreamReader sr = new StreamReader(filePath))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach(string header in headers)
                {
                    dt.Columns.Add(header);
                }

                while(!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    if(rows.Length > 1)
                    {
                        DataRow dr = dt.NewRow();
                        for(int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i].Trim();
                        }
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }
        private static DataTable ConvertXSLXtoDataTable(string fiePath, string connString)
        {
            OleDbConnection conn = new OleDbConnection(connString);
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Hoja1$]", conn);
                OleDbDataAdapter oledata = new OleDbDataAdapter();
                oledata.SelectCommand = cmd;
                DataSet ds = new DataSet();
                oledata.Fill(ds);

                dt = ds.Tables[0];
            }
            catch(Exception e) when(e is InvalidOperationException || e is OleDbException)
            {
                Debug.WriteLine(e.Message);
            }
            finally
            {
                conn.Close();
            }

            return dt;
        }
        #endregion
    }
}