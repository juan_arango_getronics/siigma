﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SIIGMA.Constants;
using SIIGMA.Services;

namespace SIIGMA.Controllers
{
    [RoutePrefix("tiempos")]
    public class TiemposController : Controller
    {
        #region Fields
        private readonly ILoggingService _loggingService;
        private readonly ITiemposService _tiemposService;
        #endregion

        #region Constructors
        public TiemposController(ILoggingService logingService, ITiemposService tiemposService)
        {
            this._loggingService = logingService;
            this._tiemposService = tiemposService;
        }
        #endregion

        [Route("cambiarestado", Name = TiemposControllerRoute.CambiarEstado)]
        public async Task<ActionResult> CambiarEstado(string estado)
        {
            try
            {
                if (string.IsNullOrEmpty(estado))
                    throw new ArgumentNullException("estado");

                await _tiemposService.CambiarEstado(estado);
                return Json(new { success = true, estado = estado }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _loggingService.Log(e);
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}