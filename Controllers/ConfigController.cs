﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web.Mvc;
using SIIGMA.Constants;
using SIIGMA.DTO;
using SIIGMA.Services;

namespace SIIGMA.Controllers
{
    [RoutePrefix("config")]
    public class ConfigController : Controller
    {
        #region Fields
        private readonly IRolesService _roleService;
        private readonly ILoggingService _logginService;
        private readonly IConfigService _configService;
        #endregion

        #region Constructs
        public ConfigController(IRolesService roleService,
            ILoggingService logginService,
            IConfigService configService)
        {
            this._roleService = roleService;
            this._logginService = logginService;
            this._configService = configService;
        }
        #endregion

        [Route("index", Name = ConfigControllerRoute.GetIndex)]
        public ActionResult Index()
        {
            return this.View(ConfigControllerAction.Index);
        }

        [Route("getusernames", Name = ConfigControllerRoute.GetUsernames)]
        public async Task<ActionResult> GetUsernames(JqGridDefaultAttrs model)
        {
            try
            {
                var list = await _roleService.GetConcatUserRolesAsync(model);
                int index = model.page - 1;
                int size = model.rows;

                int totalReg = list.Count;
                int pages = (int) Math.Ceiling((float) totalReg / model.rows);

                list = list.AsQueryable().Skip(index * size).Take(size).ToList();

                return Json(new { total = pages, model.page, records = totalReg, rows = list }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [Route("getallroles", Name = ConfigControllerRoute.GetAllRoles)]
        public async Task<ActionResult> GetAllRoles(JqGridDefaultAttrs model)
        {
            try
            {
                var list = _roleService.GetAll();
                int index = model.page - 1;
                int size = model.rows;

                int totalReg = list.Count;
                int pages = (int) Math.Ceiling((float) totalReg / model.rows);

                list = list.AsQueryable().Skip(index * size).Take(size).ToList();

                await Task.Delay(TimeSpan.FromSeconds(1));
                return Json(new { total = pages, model.page, records = totalReg, rows = list }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [Route("eliminarusuario", Name = ConfigControllerRoute.EliminarUsuario)]
        public async Task<ActionResult> EliminarUsuario(string usuario)
        {
            try
            {
                if(string.IsNullOrEmpty(usuario))
                    throw new ArgumentNullException("usuario");

                await _roleService.EliminarUsuario(usuario);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("agregarusuario", Name = ConfigControllerRoute.AgregarUsuario)]
        public async Task<ActionResult> AgregarUsuario(string usuario, string[] roles)
        {
            try
            {
                if(string.IsNullOrEmpty(usuario))
                    throw new ArgumentNullException("usuario");
                if(roles.Length < 1)
                    throw new ArgumentNullException("roles");

                await _roleService.AgregarUsuario(usuario, roles);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("getcolas", Name = ConfigControllerRoute.GetColas)]
        public async Task<ActionResult> GetColas(JqGridDefaultAttrs model)
        {
            try
            {
                var list = await _configService.GetColas();

                int index = model.page - 1;
                int size = model.rows;

                int totalReg = list.Count;
                int pages = (int) Math.Ceiling((float) totalReg / model.rows);

                list = list.AsQueryable().Skip(index * size).Take(size).ToList();
                return Json(new { total = pages, model.page, records = totalReg, rows = list }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("agregarcola", Name = ConfigControllerRoute.AgregarCola)]
        public async Task<ActionResult> AgregarCola(string cola, string descripcion)
        {
            try
            {
                if(string.IsNullOrEmpty(cola))
                    throw new ArgumentNullException("cola");
                if(string.IsNullOrEmpty(descripcion))
                    throw new ArgumentNullException("descripcion");

                cola = cola.Replace(" ", "_").ToUpper();
                await _configService.AgregarCola(cola, descripcion);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("eliminarcola", Name = ConfigControllerRoute.EliminarCola)]
        public async Task<ActionResult> EliminarCola(string cola)
        {
            try
            {
                if(string.IsNullOrEmpty(cola))
                    throw new ArgumentNullException("cola");

                await _configService.EliminarCola(cola);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("getmaestrosselect2", Name = ConfigControllerRoute.GetMaestrosSelect2)]
        public async Task<ActionResult> GetMaestrosSelect2()
        {
            try
            {
                var reglas = await _configService.GetReglas();
                var usuarios = await _configService.GetUsuarios();

                List<string> usuariosCorregidos = new List<string>();
                foreach(var user in usuarios)
                {
                    string usuario = user.Substring(user.LastIndexOf('\\') + 1);
                    usuariosCorregidos.Add(usuario);
                }

                return Json(new { success = true, maestros = new { reglas = reglas, usuarios = usuariosCorregidos } }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("getcolasreglasusuarios", Name = ConfigControllerRoute.GetColasReglasUsuarios)]
        public async Task<ActionResult> GetColasReglasUsuarios()
        {
            try
            {
                var info = await _configService.GetColasReglasUsuarios();

                return Json(new { success = true, model = info }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("getcolastabla", Name = ConfigControllerRoute.GetColasTabla)]
        public async Task<ActionResult> GetColasTabla()
        {
            try
            {
                var info = await _configService.GetColas();

                return Json(new { success = true, model = info }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("guardarreglas", Name = ConfigControllerRoute.GuardarReglas)]
        public async Task<ActionResult> GuardarReglas(string cola, string regla)
        {
            try
            {
                if(string.IsNullOrEmpty(cola))
                    throw new ArgumentNullException("cola");
                if(string.IsNullOrEmpty(regla))
                    throw new ArgumentNullException("regla");

                await _configService.AgregarReglasCola(cola, regla);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("guardarusuario", Name = ConfigControllerRoute.GuardarUsuario)]
        public async Task<ActionResult> GuardarUsuario(string cola, string usuario)
        {
            try
            {
                if(string.IsNullOrEmpty(cola))
                    throw new ArgumentNullException("cola");
                if(string.IsNullOrEmpty(usuario))
                    throw new ArgumentNullException("usuario");

                await _configService.AgregarUsuarioCola(cola, usuario);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("eliminarreglacola", Name = ConfigControllerRoute.EliminarReglaCola)]
        public async Task<ActionResult> EliminarReglaCola(string cola, string regla)
        {
            try
            {
                if(string.IsNullOrEmpty(cola))
                    throw new ArgumentNullException("cola");
                if(string.IsNullOrEmpty(regla))
                    throw new ArgumentNullException("regla");

                await _configService.EliminarReglasCola(cola, regla);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Route("eliminarusuariocola", Name = ConfigControllerRoute.EliminarUsuarioCola)]
        public async Task<ActionResult> EliminarUsuarioCola(string cola, string usuario)
        {
            try
            {
                if(string.IsNullOrEmpty(cola))
                    throw new ArgumentNullException("cola");
                if(string.IsNullOrEmpty(usuario))
                    throw new ArgumentNullException("usuario");

                await _configService.EliminarUsuarioCola(cola, usuario);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

    }
}