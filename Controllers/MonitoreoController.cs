﻿using SIIGMA.Constants;
using System.Threading.Tasks;
using System.Web.Mvc;
using SIIGMA.Services;
using System;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using SIIGMA.Models;
using SIIGMA.DTO;

namespace SIIGMA.Controllers
{
    [RoutePrefix("monitoreo")]
    public class MonitoreoController : Controller
    {
        #region Fields
        private readonly IRolesService _rolesService;
        private readonly ILoggingService _logginService;
        private readonly IMonitoreoService _monitoreoService;
        private readonly ITiemposService _tiemposService;
        private readonly IReglasService _reglasService;
        #endregion

        #region Contructors
        public MonitoreoController(
            IRolesService rolesService, 
            ILoggingService logginService,
            IMonitoreoService monitoreoService,
            ITiemposService tiemposService,
            IReglasService reglasService)
        {
            this._rolesService = rolesService;
            this._logginService = logginService;
            this._monitoreoService = monitoreoService;
            this._tiemposService = tiemposService;
            this._reglasService = reglasService;
        }
        #endregion

        [Route("getinfocliente", Name = MonitoreoControllerRoute.GetInfoCliente)]
        public ActionResult GetInfoCliente(long documento)
        {
            try
            {
                if (documento <= 0)
                    throw new ArgumentNullException("documento");

                var infoCliente = _monitoreoService.GetInfoCliente(documento);

                return Json(new { success = true, model = infoCliente }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                _logginService.Log(ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [Route("getalerta", Name = MonitoreoControllerRoute.GetAlerta)]
        public async Task<ActionResult> GetAlerta(JqGridDefaultAttrs model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            try
            {
                var regla = string.Empty;
                var transacciones = _monitoreoService.GetTransacciones(out regla);
                if(!transacciones.Any())
                    return Json(new { total = 0, model.page, records = 0, rows = "" }, JsonRequestBehavior.AllowGet);

                await _monitoreoService.CambiarEstadoReglasGAsync(transacciones.Select(f => f.LLAVE).ToList(), regla);

                int index = model.page - 1;
                int size = model.rows;

                int totalReg = transacciones.Count;
                int pages = (int) Math.Ceiling((float) totalReg / (float) model.rows);

                transacciones = transacciones.AsQueryable().Skip(index * size).Take(size).ToList();

                return Json(new { total = pages, model.page, records = totalReg, rows = transacciones }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                _logginService.Log(ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [Route("getdetallealerta", Name = MonitoreoControllerRoute.GetDetalleAlerta)]
        public ActionResult GetDetalleAlerta(string llave, string regla)
        {
            try
            {
                if(string.IsNullOrEmpty(llave))
                    throw new ArgumentNullException("llave");
                if(string.IsNullOrEmpty(regla))
                    throw new ArgumentNullException("regla");

                var detalle = _monitoreoService.GetDetalleAlerta(llave, regla);
                return Json(new { detalle }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [Route("gethistorico", Name = MonitoreoControllerRoute.GetHistorico)]
        public ActionResult GetHistorico(JqGridDefaultAttrs model, string documento)
        {
            try
            {
                if (model == null)
                    throw new ArgumentNullException("model");
                if (string.IsNullOrEmpty(documento))
                    throw new ArgumentNullException("documento");

                long doc = long.Parse(documento);
                var historicos = _monitoreoService.GetHistorico(doc);

                int index = model.page - 1;
                int size = model.rows;

                int totalReg = historicos.Count;
                int pages = (int)Math.Ceiling((float)totalReg / (float)model.rows);

                historicos = historicos.AsQueryable().Skip(index * size).Take(size).ToList();

                return Json(new { total = pages, model.page, records = totalReg, rows = historicos }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [Route("getcolasasignadas", Name = MonitoreoControllerRoute.GetColasAsignadas)]
        public async Task<ActionResult> GetColasAsignadas()
        {
            try
            {
                var colas = await _monitoreoService.GetColasAsignadasAsync();
                return Json(new { success = true, model = colas }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }

        }

        [Route("registrargestion", Name = MonitoreoControllerRoute.RegistrarGestion)]
        public async Task<ActionResult> RegistrarGestion()
        {
            try
            {
                var parameters = Request.Form;
                var alertasSplitted = parameters["alertas"].Split(',').ToList();
                foreach(string llave in alertasSplitted)
                {
                    if(_reglasService.AlmacenaBases(parameters["regla"]))
                    {
                        var detalleAlerta = _monitoreoService.GetDetalleAlerta(llave, parameters["regla"]);

                        // Mapea el detalle de la alerta en la entidad Info_Producto del cliente
                        var infoProducto = await _monitoreoService.MapearInfoBasesAsync<InfoProducto>(detalleAlerta, parameters);
                        await _monitoreoService.GuardarSegbanAsync(infoProducto);

                        // Mapea el detalle de la alerta en la entidad de Info_Transacciones del cliente
                        var infoTransaccion = await _monitoreoService.MapearInfoBasesAsync<InfoTransaccion>(detalleAlerta, parameters);
                        await _monitoreoService.GuardarSegbanAsync(infoTransaccion);

                        // Valida si la regla inserta datos del receptor.
                        if(_reglasService.RegistraReceptor(parameters["regla"]))
                        {
                            // Mapea el detalle de la alerta en la entidad Info_Producto del receptor
                            var infoProductoReceptor = await _monitoreoService.MapearInfoBasesAsync<InfoTransaccion>(detalleAlerta, parameters, true);
                            await _monitoreoService.GuardarSegbanAsync(infoProductoReceptor);

                            // Mapea el detalle de la alerta en la entidad de Infor_Transacciones del receptor.
                            var infoTransaccionReceptor = await _monitoreoService.MapearInfoBasesAsync<InfoTransaccion>(detalleAlerta, parameters, true);
                            await _monitoreoService.GuardarSegbanAsync(infoTransaccionReceptor);
                        }
                    }
                }
                // Almacena la información de la gestion en las tablas de DBMINERIA
                await _monitoreoService.GuardarInfoMineriaAsync(alertasSplitted, parameters["regla"], parameters["GESTION"], DateTime.Now, parameters["OBSERVACIONES"]);

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                _logginService.Log(e);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }
    }
}