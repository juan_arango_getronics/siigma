﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.DTO
{
    public class JqGridDefaultAttrs
    {
        public JqGridDefaultAttrs() { }

        public string sidx { get; set; }
        public string sord { get; set; }
        public int page { get; set; }
        public int rows { get; set; }
        public string sender { get; set; }
        public string searchField { get; set; }
        public string searchstring { get; set; }
        public string searchOper { get; set; }
    }
}