﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.DTO
{
    public class SelectItem
    {
        public SelectItem(string id, string text, bool selected)
        {
            this.id = id;
            this.text = text;
            this.selected = selected;
        }

        public string id { get; set; }
        public string text { get; set; }
        public bool selected { get; set; }
    }
}