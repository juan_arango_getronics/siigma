﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.DTO
{
    public class EstadisticasDTO
    {
        public int AlertasGestionadasHoy { get; set; }
        public int AlertasGestionadasAyer { get; set; }
        public float PorcentajeGestion { get; set; }
        public string TiempoPromedioGestion { get; set; }
        public string TiempoPromedioDisponible { get; set; }
        public string TiempoPromedioNoDisponible { get; set; }
        public Dictionary<string, int> GestionadasPorRegla { get; set; }
        public Dictionary<string, int> GestionadasAyerPorRegla { get; set; }
        public Dictionary<string, int> PendientesGestionar { get; set; }
    }
}