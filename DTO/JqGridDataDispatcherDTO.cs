﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.DTO
{
    public class JqGridDataDispatcherDTO : JqGridDefaultAttrs
    {
        public string documento { get; set; }
        public string producto { get; set; }
        public string prod_numero { get; set; }
        public string cuenta_numero { get; set; }
    }
}