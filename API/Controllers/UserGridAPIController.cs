﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Http;
using SIIGMA.Models;
using System.Linq;

namespace SIIGMA.API
{
    [RoutePrefix("usergrid")]
    public class UserGridAPIController : ApiController
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("Get")]
        public string Get(int id)
        {
            return "value";
        }

        /**public IEnumerable<object> Get()
        {
            UserRoles filter = GetFilter();

            using(AppDbSegbanContext _db = new AppDbSegbanContext())
            {
                return _db.EUserRoles.Where(f => 
                    (string.IsNullOrEmpty(filter.UserName) || f.UserName.Contains(filter.UserName)) && 
                    (string.IsNullOrEmpty(filter.RoleName) || f.RoleName.Contains(filter.RoleName))).ToArray();
            }
        }
        private UserRoles GetFilter()
        {
            NameValueCollection filter = HttpUtility.ParseQueryString(Request.RequestUri.Query);

            return new UserRoles
            {
                UserName = filter[nameof(UserRoles.UserName)],
                RoleName = filter[nameof(UserRoles.RoleName)]
            };
        }**/
    }
}