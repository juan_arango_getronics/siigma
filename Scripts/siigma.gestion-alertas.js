﻿/**
 * Módulo JavaScript para la Gestión de las Alertas 
 * @author Juan Santiago Arango
 * @version 1.0
 **/
SIIGMA.GestionAlertas = (function () {
    var intervalId = 0;
    /*
    * Objeto que encapsula los elementos DOM jQuery para acceder a ellos mas facilmente
    */
    var ctrl = {
        inps: {
            $txtSaldo: $('div#div-saldo'),
            $txtCuenta: $('div#div-cuenta'),
            $txtCanal: $('div#div-canal'),
            $txtNit: $('div#div-nit')
        },
        $frmGestion: $('form#frm-gestion'),
        $gridAlertas: $('table#grid-alertas'),
        $pagerGridAlertas: $('div#pager-alertas'),
        $gridHistorico: $('table#grid-historico'),
        $pagerGridHistorico: $('div#pager-historico'),
        $detalleAlertaList: $('dl#detalle-alerta'),
        $loadingDetalle: $('span#loading-detalle'),
        $loadingAlerta: $('span#loading-alerta'),
        $dropdownEstado: $('a#opcion-estado'),
        $btnGuardarGestion: $('button#btn-guardar-gestion'),
        $btnGuardarGestionCarret: $('button#btn-guardar-gestion-carret'),
        $colasWrapper: $('span#colas-wrapper'),
        $notificacionAlerta: $('div#notificacion-nueva-alerta')
    };
    /**
    * Objeto que encapsula los formateadores de uso general en el modulo
    */
    var formats = {
        date: function (cellvalue, options, rowObject) {
            return $.format.date(new Date(parseInt(cellvalue.substr(6))), 'HH:mm:ss');
        }
    };
    /**
     * Objeto que contiene todas las funciones Callbacks de las llamadas
     * AJAX en todo el modulo.
     */
    var callbacks = {
        detalleAlerta: function (response) {
            if (response.detalle) {
                $.each(response.detalle, function (i, v) {
                    ctrl.$detalleAlertaList.html(' ');
                    $.each(v, function (index, val) {
                        ctrl.$detalleAlertaList.append($('<dt>').html(val.Key)).append($('<dd class="text-muted">').html(val.Value));
                    });
                });
            }
            ctrl.$loadingDetalle.hide();
        }
    };
    /** 
    * Funcion invocada desde el exterior para instanciar todos los elementos en el load del document
    */
    function instanceControls() {
        /*
         * Llamada a la funcion para cargar las colas que tiene asignado el usuario, antes de cargar la primera alerta
         */
        cargarColasAsignadas();
        /*
         * Asigna los eventos a los controles
         */
        ctrl.$dropdownEstado.on('click', cambiarColorBoton);
        ctrl.$btnGuardarGestion.on('click', guardarGestion);
        /*
         * Oculta los label 'cargando' en todos los grids
         */
        ctrl.$loadingDetalle.hide();
        ctrl.inps.$txtSaldo.hide();
        ctrl.inps.$txtCuenta.hide();
        ctrl.inps.$txtCanal.hide();
        ctrl.inps.$txtNit.hide();
        /*
         * Carga las opciones por defecto de los widgets de la herramienta
         */
        toastr.options = SIIGMA.Opts.toaster;
        /**
         * Definición de Grid Alertas
        **/
        ctrl.$gridAlertas.jqGrid({
            url: SIIGMA.UrlBase + 'monitoreo/getalerta',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, label: 'Llave', name: 'LLAVE', index: 'INDEX', align: 'center', hidden: true, sortable: false },
                { key: false, label: 'Id', name: 'ID', index: 'ID', align: 'center', sortable: false },
                { key: false, label: 'Documento', name: 'DOCUMENTO', index: 'DOCUMENTO', align: 'center', sortable: false },
                { key: false, label: 'Hora alerta', name: 'HORA', index: 'HORA', align: 'center', formatter: formats.date, sortable: false },
                { key: false, label: 'Regla', name: 'REGLA', index: 'REGLA', align: 'center', sortable: false }
            ],
            pager: ctrl.$pagerGridAlertas,
            rowNum: 50,
            rowList: [50],
            height: 'auto',
            width: 1180,
            viewrecords: true,
            sortorder: 'asc',
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: true,
            sortable: false,
            beforeSelectRow: beforeSelectRowHandler,
            loadComplete: loadCompleteCallback,
        }).navGrid(ctrl.$pagerGridAlertas, { edit: false, add: false, del: false, search: false, refresh: false });

        ctrl.$gridAlertas.jqGrid('setGridWidth', ctrl.$gridAlertas.closest('.ui-jqgrid').parent().width(), true);
        /**
         * Definición de Grid Historico
        **/
        ctrl.$gridHistorico.jqGrid({
            url: SIIGMA.UrlBase + 'monitoreo/gethistorico',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: false, label: 'Documento', name: 'DOCUMENTO', index: 'DOCUMENTO', align: 'center', sortable: false },
                { key: false, label: 'Regla', name: 'REGLA', index: 'REGLA', align: 'center', sortable: false },
                { key: false, label: 'Gestion', name: 'GESTION', index: 'GESTION', align: 'center', sortable: false },
                { key: false, label: 'Fecha', name: 'FECHA', index: 'FECHA', align: 'center', sortable: false },
                { key: false, label: 'Hora', name: 'HORA', index: 'HORA', align: 'center', sortable: false },
                { key: false, label: 'Canal', name: 'CANAL', index: 'CANAL', align: 'center', sortable: false },
                { key: false, label: 'Observaciones', name: 'ALERTA_OBSERVACIONES', align: 'right', sortable: false },
                { key: false, label: 'Receptor', name: 'RECEPTOR', align: 'center', sortable: false }
            ],
            pager: ctrl.$pagerGridHistorico,
            rowNum: 50,
            rowList: [50],
            height: 'auto',
            width: 1180,
            viewrecords: true,
            sortorder: 'asc',
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
            sortable: false
        }).navGrid(ctrl.$pagerGridHistorico, { edit: false, add: false, del: false, search: false, refresh: false });

        ctrl.$gridHistorico.jqGrid('setGridWidth', ctrl.$gridHistorico.closest('.ui-jqgrid').parent().width(), true);
    }
    /*
    * Funcion callback que se ejecuta luego de que se carga completamente el grid
    */
    function loadCompleteCallback(data) {
        $('input.ui-pg-input').attr('disabled', 'disabled');
        ctrl.$loadingAlerta.hide();
        if (data.records <= 0) {
            intervalId = setInterval(recargarGridAlertas, 1000);
        } else {
            var res = SIIGMA.Tiempos.cambiarEstado('GE');
        }
    }
    /*
     * Funcion encargada de manejar la carga de alertas en el grid.
     * Cuando se va a cargar la informacion de la alerta valida si "setInterval" está activo
     * y anula el proceso.
     */
    function recargarGridAlertas() {
        ctrl.$loadingAlerta.show();
        if (intervalId > 0) {
            clearInterval(intervalId);
        }
        ctrl.$gridAlertas.jqGrid().trigger('reloadGrid');
    }
    /*
    * Metodo que maneja el evento de seleccion de fila en el grid de las alertas relacionadas.
    */
    function beforeSelectRowHandler(rowid, e) {
        var $data = $(this).jqGrid('getRowData', rowid),
            data = { regla: $data.REGLA, llave: rowid };

        actualizarFormulario(rowid);
        cargarDetalleAlerta(data);
        cargarInfoCliente($data.DOCUMENTO);
        cargarHistorico($data.DOCUMENTO);
    }
    /** 
    * Funcion que se encarga de cargar toda la informacion de contacto del cliente.
    * @param {string} doc Documento del cliente.
    */
    function cargarInfoCliente(doc) {
        var data = { documento: doc };
        $.get(SIIGMA.UrlBase + 'monitoreo/getinfocliente', data, function (response) {
            if (response.success) {
                $.each(response.model, function (index, value) {
                    if (value.indexOf('@') >= 0) {
                        value = value.replace('@', '@ ');
                    }
                    if (index === 'Tip') {
                        $('span#Tip').html(value);
                    }
                    $('dd#' + index).html(value);
                });
            } else {
                toastr['error']('Ha ocurrido un error al cargar la información del cliente.');
            }
        });
    }
    /**
    * Funcion encargada de cargar el detalle de la alerta cuando se da clic sobre la fila que se desea
    * @param {object} data Los datos capturados del GRID.
    */
    function cargarDetalleAlerta(data) {
        ctrl.$loadingDetalle.show();
        $.get(SIIGMA.UrlBase + 'monitoreo/getdetallealerta', data, callbacks.detalleAlerta);
    }
    /**
    * Metodo usado para cargar la informacion en el grid de historicos de alertas
    * @param {string} doc Documento del cliente.
    */
    function cargarHistorico(doc) {
        var postDataValues = ctrl.$gridHistorico.jqGrid('getGridParam', 'postData');
        postDataValues['documento'] = doc;
        ctrl.$gridHistorico.jqGrid().setGridParam({ postData: postDataValues, page: 1 }).trigger('reloadGrid');
    }
    /**
    * Funcion usada para cambiar el color del boton de registrar e indicar cual es el estado en que estará 
    * el usuario luego de almacenar el registro de la gestion de la alerta.
    * @param {object} e Informacion del evento.
    */
    function cambiarColorBoton(e) {
        e.preventDefault();
        if ($(this).data('estado') === 'disponible') {
            $(this).html('<i class="fa fa-circle text-success"></i> Disponible');
            ctrl.$btnGuardarGestion.attr('class', 'btn btn-danger btn-sm');
            ctrl.$btnGuardarGestionCarret.attr('class', 'btn btn-danger btn-sm dropdown-toggle');
            $(this).data('estado', 'no_disponible');
        } else {
            $(this).html('<i class="fa fa-circle text-danger"></i> No disponible');
            ctrl.$btnGuardarGestion.attr('class', 'btn btn-success btn-sm');
            ctrl.$btnGuardarGestionCarret.attr('class', 'btn btn-success btn-sm dropdown-toggle');
            $(this).data('estado', 'disponible');
        }
    }
    /**
     * Funcion que permite cargar las colas que tiene asignadas el usuario para mostrarlas en la parte
     * superior derecha de la pantalla.
     */
    function cargarColasAsignadas() {
        $.get(SIIGMA.UrlBase + 'monitoreo/getcolasasignadas', function (response) {
            if (response.success) {
                $.each(response.model, function (index, val) {
                    ctrl.$colasWrapper.append($('<span class="label label-default">').html(val)).append('&nbsp;');
                });
            }
        });
    }
    /*
     * Funcion que actualiza los campos que son requeridos para la gestion de la alerta, dependiendo
     * de la regla que generó la alerta
     */
    function actualizarFormulario(rowid) {
        var $data = ctrl.$gridAlertas.jqGrid('getRowData', rowid);
        switch ($data.REGLA) {
            case 'BMOVIL':
            case 'DELAY_FACTURANET':
            case 'INSCUENTAS':
            case 'PERSONALIZ':
                ctrl.inps.$txtSaldo.show();
                ctrl.inps.$txtCuenta.show();
                break;
            case 'MDL_CHIP':
            case 'MT_VIRTUAL':
            case 'PAGO_TJS':
            case 'PSE_PAGOS':
            case 'STF_CONSAL':
            case 'SUPLA_VIR':
                ctrl.inps.$txtSaldo.show();
                break;
            case 'CONSUL_SALDO':
                ctrl.inps.$txtSaldo.show();
                ctrl.inps.$txtCanal.show();
                break;
        }
        ctrl.$btnGuardarGestion.removeAttr('disabled');
        ctrl.$btnGuardarGestionCarret.removeAttr('disabled');

        if ($data.DOCUMENTO === '1111111111' && ($data.REGLA === 'BLOQUEO_RSA' || $data.REGLA === 'BLOQ_RSA_EMP')) {
            ctrl.inps.$txtNit.show();
        }
    }
    /*
     * Metodo que controla el evento click del boton de registrar gestion.
     */
    function guardarGestion(e) {
        e.preventDefault();

        var alertasSeleccionadas = ctrl.$gridAlertas.jqGrid('getGridParam', 'selarrrow'),
            totalAlertas = ctrl.$gridAlertas.jqGrid('getGridParam', 'records'),
            error = false;
        var alertasArray = [];

        $.each(alertasSeleccionadas, function (i, v) {
            alertasArray.push(v);
        })

        if (alertasArray.length <= 0) {
            toastr['info']('Debe de seleccionar al menos una alerta para registrar la gestión.', 'Informativo');
            return false;
        }
        $.each(ctrl.$frmGestion.find('input,select,textarea'), function (index, val) {
            var _self = $(this);
            if (_self.parent().parent().css('display') !== 'none') {
                if (monitoreoUtiles.validarNuloVacio(_self.val())) {
                    error = true;
                    return false;
                }
            }
        });
        if (error) {
            toastr['info']('Aún existen campos de la gestión pendientes por diligenciar.', 'Informativo');
            return false;
        } else {
            var data = ctrl.$frmGestion.serializeArray();
            data.push({ name: 'alertas', value: alertasArray });
            data.push({ name: 'regla', value: 'E_PREPAGO' });

            $.ajax({
                url: SIIGMA.UrlBase + 'monitoreo/registrargestion',
                data: data,
                dataType: 'json',
                type: 'POST',
                error: function () {
                    toastr['error']('Se ha presentado un error al procesar la gestión, por favor comuniquese con soporte para mas información.', 'Error');
                },
                success: function (response) {
                    ctrl.$frmGestion.trigger('reset');
                    ctrl.$btnGuardarGestion.attr('disabled', 'disabled');
                    ctrl.$btnGuardarGestionCarret.attr('disabled', 'disabled');
                    ctrl.$gridHistorico.jqGrid('clearGridData');
                    ctrl.$detalleAlertaList.html(' ');
                    $.each($('dl#info-cliente').find('dd'), function (index, val) {
                        $(this).html(' ');
                    });
                    if (alertasArray.length === totalAlertas) {
                        ctrl.$gridAlertas.jqGrid('clearGridData');
                        if (ctrl.$dropdownEstado.data('estado') === 'disponible') {
                            SIIGMA.Tiempos.cambiarEstado('DI');
                        } else {
                            SIIGMA.Tiempos.cambiarEstado('ND');
                        }
                    } else {
                        $.each(alertasArray, function (i, v) {
                            ctrl.$gridAlertas.jqGrid('delRowData', v);
                        })
                    }
                    toastr['success']('La gestión ha sido almacenada correctamente.', 'Correcto');
                }
            });
        }
    }
    /*
     * Objeto que encapsula funciones generales del modulo
     */
    monitoreoUtiles = {
        validarNuloVacio: function (str) {
            var res = (str === '' || str === ' ' || str === null ||
                str === undefined || str === 'undefined') ? true : false;
            return res;
        }
    }
    /*
     * Funcion publica que expone el metodo para setear o eliminar los intervalos en los que se va a ejecutar la consulta 
     * de alertas.
     */
    function unsetInterval(start) {
        if (start) {
            intervalId = setInterval(recargarGridAlertas, 1000);
        } else {
            clearInterval(intervalId);
        }
    }
    /**
    * Funcion de iniciacion del modulo 'visible'
    */
    function init() {
        instanceControls();
    }
    /**
    * Retorna el metodo 'init' (public) del modulo, el cual se invocara en el load del document
    */
    return {
        init: init,
        unsetInterval: unsetInterval
    };
}());