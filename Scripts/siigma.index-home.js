﻿$(document).ready(function () {
    $('a[data-toggle="tab"]:first').tab('show');

    $('a[data-toggle="tab"]').on('show.bs.tab', tabClicked);

    function tabClicked(e) {
        debugger;
        if ($(e.target).attr('href') === '#bases') {
            var bg = SIIGMA.BasesGestion;
            bg.init();
        }
        $('a[data-toggle="tab"]').off('show.bs.tab', tabClicked);
    }

    $('[data-toggle="tooltip"]').tooltip();
});