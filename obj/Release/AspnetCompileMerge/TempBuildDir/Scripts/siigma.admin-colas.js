﻿SIIGMA.AdminColas = (function () {
    controles = {
        $colasGrid: $('table#grid-colas'),
        $colasModal: $('div#modal-cola'),
        $btnAgregarCola: $('button#btn-nueva-cola'),
        $btnGuardarInfoCola: $('button#btn-guardar-cola'),
        $txtCola: $('input#queue-name'),
        $txtDescripcion: $('input#queue-desc'),
        $tablaColasUsuario: $('table#tabla-colas-usuarios')
    }

    function instanciarControles() {
        toastr.options = SIIGMA.Opts.toaster;

        cargarColasUsuariosReglas();

        controles.$colasGrid.jqGrid({
            url: SIIGMA.UrlBase + '/config/getcolas',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, label: 'Cola', name: 'QueueName', index: 'QueueName', align: 'center', hidden: false, width: 40 },
                { key: false, label: 'Descripcion', name: 'Description', index: 'Description', align: 'center', sortable: false, search: false, width: 80 },
                { key: false, label: 'Acciones', name: '', index: '', align: 'center', sortable: false, search: false, formatter: formatos.acciones, width: 20 }
            ],
            pager: jQuery('#pager-colas'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'QueueName',
            sortorder: 'asc',
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
            gridComplete: colasGridsEvents.colasGridCompleteEventHandler
        }).navGrid('#pager-colas', { edit: false, add: false, del: false, search: false, refresh: false });

        utiles.setGridWidth(controles.$colasGrid);

        controles.$btnAgregarCola.on('click', function (e) {
            e.preventDefault();
            $('form#frm-nueva-cola').trigger('reset');
            controles.$colasModal.modal('show');
        });

        controles.$btnGuardarInfoCola.on('click', function (e) {
            e.preventDefault();
            if (controles.$txtCola.val() === '') {
                toastr['info']('Por favor ingrese el nombre de la cola que desea registrar.', 'Informativo');
            } else if (controles.$txtDescripcion.val() === '') {
                toastr['info']('Por favor ingrese la descripción de la cola que desea registrar.', 'Informativo');
            } else {
                $.ajax({
                    url: SIIGMA.UrlBase + '/config/agregarcola',
                    data: { cola: controles.$txtCola.val(), descripcion: controles.$txtDescripcion.val() },
                    method: 'POST',
                    dataType: 'json',
                    async: true,
                    success: function (response) {
                        if (response.success) {
                            toastr['success']('La información ha sido registrada correctamente', 'Correcto');
                            controles.$colasGrid.jqGrid().trigger('reloadGrid');
                            controles.$colasModal.modal('hide');
                        }
                    },
                    error: function (response) {
                        toastr['error']('Se ha presentado un error al procesar la solicitud, por favor comuniquese con soporte', 'Error');
                    }
                })
            }
        });

        $('td#pager-colas_left').remove();
        $('td#pager-colas_center').remove();
    }

    colasGridsEvents = {
        colasGridCompleteEventHandler: function () {
            utiles.setGridWidth($(this));

            $('span.eliminar-cola').on('click', function (e) {
                e.preventDefault();
                var cola = $(this).data('cola');
                if (confirm('¿Está seguro(a) que desea eliminar la cola "' + cola + '"? \n Esta acción eliminará los usuarios y las reglas asignadas a esta cola. \n Esta acción no puede deshacerse')) {
                    $.post(SIIGMA.UrlBase + '/config/eliminarcola', { cola: cola }, function (response) {
                        if (response.success) {
                            toastr['success']('El registro ha sido almacenado correctamente', 'Correcto');
                            controles.$colasGrid.jqGrid().trigger('reloadGrid');
                        }
                    });
                }
            });
        }
    }

    utiles = {
        setGridWidth: function (element) {
            var $width = element.closest('.ui-jqgrid').parent().width();
            element.jqGrid('setGridWidth', $width, true);
        }
    }

    formatos = {
        acciones: function (cellvalue, options, rowObject) {
            return '<div><span class="text-danger fa fa-trash fa-2x eliminar-cola" aria-hidden="true" title="Eliminar" style="cursor: pointer;" data-cola="' + rowObject.QueueName + '"></span></div>'
        }
    }

    function cargarColasUsuariosReglas() {
        controles.$tablaColasUsuario.hide();
        controles.$tablaColasUsuario.find('tbody').html(' ');

        var maestros = [];
        $.ajax({ url: SIIGMA.UrlBase + '/config/getmaestrosselect2', dataType: 'json', method: 'GET', async: false, success: function (response) { if (response.success) { maestros = response.maestros } } });

        var colasReglas = [];
        $.ajax({ url: SIIGMA.UrlBase + '/config/getcolasreglasusuarios', dataType: 'json', method: 'GET', async: false, success: function (response) { if (response.success) { colasReglas = response.model } } });

        var listaColas = [];
        $.ajax({ url: SIIGMA.UrlBase + '/config/getcolastabla', dataType: 'json', method: 'GET', async: false, success: function (response) { if (response.success) { listaColas = response.model } } });
        $.each(listaColas, function (i, v) {
            var newContent = '<tr><td>' + v.QueueName + '</td><td><select class="form-control input-sm select2-reglas select2-input" multiple="multiple" style="width: 100%;" data-cola="' + v.QueueName + '"></select></td><td><select class="form-control input-sm select2-usuarios select2-input" multiple="multiple" style="width: 100%;" data-cola="' + v.QueueName + '"></select></td></tr>'
            controles.$tablaColasUsuario.find('tbody').append(newContent);
        });
        $.each(maestros.reglas, function (i, v) {
            $('.select2-reglas').append('<option value="' + v + '" >' + v + '</option>');
        });
        $.each(maestros.usuarios, function (i, v) {
            $('.select2-usuarios').append('<option value="' + v + '" >' + v + '</option>');
        });
        $.each(colasReglas, function (i, v) {
            var cola = v.Cola;
            var reglas = v.Reglas === null ? [] : v.Reglas.split(',');
            $.each(reglas, function (i, v) {
                $('select.select2-reglas[data-cola="' + cola + '"]').find('option[value="' + v.trim() + '"]').attr('selected', 'selected');
            });
            var usuarios = v.Usuarios === null ? [] : v.Usuarios.split(',');
            $.each(usuarios, function (i, v) {
                $('select.select2-usuarios[data-cola="' + cola + '"]').find('option[value="' + v.trim() + '"]').attr('selected', 'selected');
            });
        });

        $('select.select2-reglas').select2({ theme: 'bootstrap', tags: true, language: 'es' });
        $('select.select2-usuarios').select2({ theme: 'bootstrap', tags: true, language: 'es' });

        $('select.select2-input').on('select2:select', select2ItemSelectedHandler);
        $('select.select2-input').on('select2:unselect', select2ItemUnselectHandler);

        controles.$tablaColasUsuario.show();
    }

    function select2ItemSelectedHandler(e) {
        var element = $(e.target);
        var cola = element.data('cola');
        if (element.hasClass('select2-reglas')) {
            var regla = e.params.data.id;
            $.post(SIIGMA.UrlBase + '/config/guardarreglas', { cola: cola, regla: regla }, function (response) {
                if (response.success) {
                    toastr['success']('La informacion ha sido almacenada correctamente', 'Correcto');
                }
            });
        } else {
            var usuario = e.params.data.id;
            $.post(SIIGMA.UrlBase + '/config/guardarusuario', { cola: cola, usuario: usuario }, function (response) {
                if (response.success) {
                    toastr['success']('La informacion ha sido almacenada correctamente', 'Correcto');
                }
            });
        }
        cargarColasUsuariosReglas();
    }

    function select2ItemUnselectHandler(e) {
        var element = $(e.target);
        var cola = element.data('cola');
        if (element.hasClass('select2-reglas')) {
            var regla = e.params.data.id;
            $.post(SIIGMA.UrlBase + '/config/eliminarreglacola', { cola: cola, regla: regla }, function (response) {
                if (response.success) {
                    toastr['success']('La informacion ha sido almacenada correctamente', 'Correcto');
                }
            });
        } else {
            var usuario = e.params.data.id;
            $.post(SIIGMA.UrlBase + '/config/eliminarusuariocola', { cola: cola, usuario: usuario }, function (response) {
                if (response.success) {
                    toastr['success']('La informacion ha sido almacenada correctamente', 'Correcto');
                }
            });
        }
        cargarColasUsuariosReglas();
    }

    function iniciar() {
        instanciarControles();
    }

    return {
        iniciar: iniciar
    }

}());