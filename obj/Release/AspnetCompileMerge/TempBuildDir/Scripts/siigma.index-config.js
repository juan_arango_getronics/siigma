﻿$(document).ready(function () {
    $('a[data-toggle="tab"]:first').tab('show');

    $('a[data-toggle="tab"]').on('show.bs.tab', tabClicked);

    function tabClicked(e) {
        if ($(e.target).attr('href') === '#queues') {
            var ac = SIIGMA.AdminColas;
            ac.iniciar();
        }
        $('a[data-toggle="tab"]').off('show.bs.tab', tabClicked);
    }

    var au = SIIGMA.AdminUsuarios;
    au.init();
});