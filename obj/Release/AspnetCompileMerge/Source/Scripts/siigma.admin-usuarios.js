﻿SIIGMA.AdminUsuarios = (function () {
    adminControles = {
        $gridUsuarios: $('table#grid-usuarios'),
        $pagerUsuarios: $('div#pager-usuarios'),
        $gridRoles: $('table#grid-roles'),
        $pagerRoles: $('div#pager-roles'),
        $usuarioModal: $('div#modal-usuario'),
        $eliminarModal: $('div#modal-eliminar'),
        $btnAgregarUsuario: $('button#btn-agregar-usuario'),
        $btnGuardarInfoUsuario: $('button#btn-guardar-usuario'),
        $txtUsuario: $('input#user-name')
    }

    function adminInstanceControls() {
        toastr.options = SIIGMA.Opts.toaster;

        adminControles.$gridUsuarios.jqGrid({
            url: SIIGMA.UrlBase + '/config/getusernames',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, label: 'Nombre Usuario', name: 'UserName', index: 'UserName', align: 'center', hidden: false, width: 40 },
                { key: false, label: 'Roles Asignados', name: 'RoleName', index: 'RoleName', align: 'center', formatter: formats.roles, sortable: false, search: false, width: 100 },
                { key: false, label: 'Acciones', name: '', index: '', align: 'center', sortable: false, search: false, formatter: formats.acciones, width: 20 }
            ],
            pager: jQuery('#pager-usuarios'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'UserName',
            sortorder: 'asc',
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
            gridComplete: adminGridsEvents.usuariosGridCompleteEventHandler,
        }).navGrid('#pager-usuarios', { edit: false, add: false, del: false, search: true, refresh: true }, {}, {}, {}, { multipleSearch: false, multipleGroup: false, showQuery: false, sopt: ['cn'], defaultSearch: 'cn' });

        adminControles.$gridRoles.jqGrid({
            url: SIIGMA.UrlBase + '/config/getallroles',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, label: 'Nombre Rol', name: 'Name', index: 'Name', align: 'center' },
                { key: false, label: 'Descripcion', name: 'Description', index: 'Description', align: 'center' }
            ],
            pager: jQuery('#pager-roles'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'Name',
            sortorder: 'asc',
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
        }).navGrid('#pager-roles', { edit: false, add: false, del: false, search: false, refresh: true });

        utils.setGridWidth(adminControles.$gridUsuarios);
        utils.setGridWidth(adminControles.$gridRoles);

        adminControles.$btnAgregarUsuario.on('click', function (e) {
            e.preventDefault();
            $('form#frm-nuevo-usuario').trigger('reset');
            adminControles.$usuarioModal.modal('show');
        })

        adminControles.$btnGuardarInfoUsuario.on('click', function (e) {
            e.preventDefault();
            var user = adminControles.$txtUsuario.val();
            var rols = [];
            $.each($('input[type="checkbox"].role:checked'), function (i, v) {
                rols.push($(this).val());
            });
            if (rols.length < 1) {
                toastr['info']('Debe seleccionar al menos un rol para asignarle al usuario', 'Información');
            } else {
                $.ajax({
                    url: SIIGMA.UrlBase + '/config/agregarusuario',
                    data: { usuario: user, roles: rols },
                    dataType: 'json',
                    method: 'POST',
                    async: true,
                    success: function (response) {
                        if (response.success) {
                            toastr['success']('El registro ha sido almacenado correctamente', 'Correcto');
                            adminControles.$gridUsuarios.jqGrid().trigger('reloadGrid');
                            adminControles.$usuarioModal.modal('hide');
                        }
                    },
                    error: function (response) {
                        toastr['error']('Se ha presentado un error al procesar la solicitud, comuniquese con soporte', 'Fallo');
                    }
                });
            }
        });
    }

    formats = {
        roles: function (cellvalue, options, rowObject) {
            var temp = '';
            var roles = cellvalue.split(',');
            $.each(roles, function (i, v) {
                if (v.trim() === 'Administrador') {
                    temp += '<span class="label label-primary">' + v.trim() + '</span>&nbsp;'
                } else {
                    temp += '<span class="label label-default">' + v.trim() + '</span>&nbsp;'
                }
            });
            return temp;
        },
        acciones: function (cellvalue, options, rowObject) {
            return '<div><span class="text-primary fa fa-pencil-square fa-2x" aria-hidden="true" title="Editar" style="cursor: pointer;"></span>&nbsp;<span class="text-danger fa fa-trash fa-2x link-eliminar" aria-hidden="true" title="Eliminar" style="cursor: pointer;" data-usuario="' + rowObject.UserName + '"></span></div>'
        }
    }

    adminGridsEvents = {
        usuariosGridCompleteEventHandler: function () {
            $('i.text-primary').click(function (e) {
                ctr.$usuarioModal.modal('show');
            });

            $('span.link-eliminar').on('click', function (e) {
                e.preventDefault();
                var user = $(this).data('usuario');
                if (confirm('¿Está seguro(a) que desea eliminar el usuario "' + user + '"?')) {
                    $.post(SIIGMA.UrlBase + '/config/eliminarusuario', { usuario: user }, function (response) {
                        if (response.success) {
                            toastr['success']('El registro ha sido almacenado correctamente', 'Correcto');
                            ctr.$gridUsuarios.jqGrid().trigger('reloadGrid');
                        }
                    });
                }
            });
        },
    }

    utils = {
        setGridWidth: function (element) {
            var $width = element.closest('.ui-jqgrid').parent().width();
            element.jqGrid('setGridWidth', $width, true);
        }
    }

    function init() {
        adminInstanceControls();
    }

    return {
        init: init
    }
}());