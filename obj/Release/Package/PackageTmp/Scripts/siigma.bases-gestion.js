﻿SIIGMA.BasesGestion = (function () {
    /*
     * Declaración de variables privadas usadas en el objeto jQuery
     * y para para tener un acceso rápido a los objetos en el DOM
     */
    $gridBloqueos = $('table#grid-bloqueos'),
    $gridTransacciones = $('table#grid-trx'),
    $gridNovedades = $('table#grid-novedades'),
    $gridMarcaciones = $('table#grid-marcaciones'),
    $txtDoc = $('input#txt-doc'),
    $btnBuscarDoc = $('button#btn-buscar-doc'),
    $spanUser = $('span#username'),
    $divData = $('div#data-div'),
    $afToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]'),
    $btnCargar = $('button#btnCargar'),
    $frmCargar = $('form#frm-cargar'),
    /*
     * Método que enlaza los metodos a los eventos change y click 
     * de los campos de documento y boton "Buscar"
     */
    basesInstanciarEventos = function () {
        $txtDoc.change(reloadGridBloqueos);
        $btnBuscarDoc.click(reloadGridBloqueos);
        $frmCargar.submit(function (e) {
            var dataString, contentType, processData;
            e.preventDefault();
            e.stopPropagation();
            var action = $(this).attr('action');
            if (document.getElementById('MasivoTrx').files.length === 0) {
                toastr['error']('Debe seleccionar un archivo para cargar las transacciones.', 'Error!');
                return;
            }
            
            if ($(this).attr('enctype') === 'multipart/form-data') {
                dataString = new FormData($('form#frm-cargar').get(0));
                contentType = false;
                processData = false;
            }
            $('label#lblMasivoTrx').attr('disabled', 'disabled');
            $('button#btnCargar').attr('disabled', 'disabled');
            $btnCargar.button('loading');
            $.ajax({
                url: action,
                type: 'POST',
                data: dataString,
                dataType: 'json',
                async: true,
                contentType: contentType,
                processData: processData,
                success: function (response) {
                    if (response.success) {
                        $gridTransacciones.jqGrid().trigger('reloadGrid');
                        toastr['success']('La información ha sido cargada exitosamente.', 'Correcto!');
                        $('form#frm-cargar').get(0).reset();
                        $('label#lblMasivoTrx').removeAttr('disabled');
                        $('button#btnCargar').removeAttr('disabled');
                        $btnCargar.button('reset');
                    }
                },
                error: function (response) {
                    toastr['error']('Se ha presentado un error al procesar la solicitud, por favor contacte al administrador.', 'Error!');
                }
            });
        })
    },
    /*
     * Metodo para instanciar los controles de la pagina
     * jqGrid, toastr
     */
    instanceControls = function () {
        /*
         * Cargar las opciones para el plugin TOASTR
         */
        toastr.options = SIIGMA.Opts.toaster;
        /*
         * Intancia el primer jqGrid de la pagina.
         * Grid: "Bloqueos"
         * Selector: $('table#grid-bloqueos')
         */
        $gridBloqueos.jqGrid({
            url: SIIGMA.UrlBase + 'bases/listarproductos',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, label: 'Id', name: 'Id', index: 'Id', hidden: true },
                { key: false, label: 'Documento', name: 'ID_CLIENTE', index: 'ID_CLIENTE', hidden: true, editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Tipo producto', name: 'TIPO_PRODUCTO', index: 'TIPO_PRODUCTO', editable: true, edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_prod' }, align: 'center', formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Número producto', name: 'NUMERO_PRODUCTO', index: 'NUMERO_PRODUCTO', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true, custom: true, custom_func: function (val) { if (val.length === 16) return [true, null]; else return [false, 'Numero producto debe contener 16 digitos']; } }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Número cuenta', name: 'NUMERO_CUENTA', index: 'NUMERO_CUENTA', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true, custom: true, custom_func: function (val) { if (val.length === 11) return [true, null]; else return [false, 'Numero cuenta debe contener 11 digitos']; } }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Saldo disponible', name: 'SALDO_DISPONIBLE', index: 'SALDO_DISPONIBLE', editable: true, formatter: 'currency', editrules: { edithidden: true, required: true, number: true, integer: true }, formatoptions: { decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ " }, align: 'center', formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Usuario monitoreo', name: 'USUARIO_MONITOREO', index: 'USUARIO_MONITOREO', editable: true, align: 'center', formoptions: { elmprefix: '(*) ' }, editoptions: { defaultValue: function () { return $divData.data('usuario'); }, readonly: 'readonly' } },
                { key: false, label: 'Tipo bloqueo', name: 'TIPO_BLOQUEO', index: 'TIPO_BLOQUEO', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_bloqueo' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Observaciones', name: 'OBSERVACIONES', index: 'OBSERVACIONES', editable: true, edittype: 'textarea', editoptions: { rows: '3', cols: '30' }, hidden: true, align: 'center', editrules: { edithidden: true, required: false }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Cliente contactado', name: 'CLIENTE_CONTACTADO', index: 'CLIENTE_CONTACTADO', editable: true, edittype: 'select', editoptions: { value: 'SI:SI; NO:NO', required: true }, align: 'center', formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Fecha ingreso', name: 'FECHA_INGRESO', index: 'FECHA_INGRESO', editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: 'Hora ingreso', name: 'HORA_INGRESO', index: 'HORA_INGRESO', editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "HH:mm:ss"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' hh:mm:ss' } },
                { key: false, label: 'Usuario ingreso', name: 'USUARIO_INGRESO', index: 'USUARIO_INGRESO', editable: true, align: 'center', formatter: 'text', editoptions: { defaultValue: function () { return $divData.data('usuario'); }, readonly: 'readonly' }, editrules: { edithidden: true, required: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: '', name: '__RequestVerificationToken', index: '__RequestVerificationToken', editable: true, hidden: true, editoptions: { defaultValue: function () { return $afToken.val(); } } }
            ],
            pager: jQuery('#pager-bloqueos'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'FECHA_INGRESO',
            sortorder: 'desc',
            postData: { documento: $('input#txt-doc').val() },
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
            subGrid: true,
            subGridRowExpanded: basesGridsEvents.subGridRowExpandedEventHandler,
            gridComplete: basesGridsEvents.bloqueosGridCompleteEventHandler,
            beforeSelectRow: basesGridsEvents.bloqueosBeforeSelectRowEventHandler,
            onSortCol: basesGridsEvents.sortColCallback
        }).navGrid('#pager-bloqueos', { edit: true, add: true, del: false, search: true, refresh: true }, gridOptions.bloqueosAjaxOptions, gridOptions.bloqueosAjaxOptions, {}, { multipleSearch: false, multipleGroup: false, showQuery: false, sopt: ['eq', 'ne'], defaultSearch: 'eq'});

        $gridBloqueos.jqGrid('setGridWidth', $gridBloqueos.closest('.ui-jqgrid').parent().width(), true);

        /*
         * Intancia el segundo jqGrid de la pagina
         * Grid: "Transacciones"
         * Selector: $('table#grid-trx')
         */
        $gridTransacciones.jqGrid({
            url: SIIGMA.UrlBase + 'bases/jqgriddispatcher',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, name: 'Id', index: 'Id', hidden: true },
                { key: false, label: 'Documento', name: 'ID_CLIENTE', index: 'ID_CLIENTE', editable: true, hidden: true, align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('id-cliente'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Tipo Producto', name: 'TIPO_PRODUCTO', index: 'TIPO_PRODUCTO', editable: true, width: 80, align: 'center', hidden: true, editrules: { edithidden: true, required: true }, editoptions: { defaultValue: function () { return $divData.data('producto'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Producto', name: 'NUMERO_PRODUCTO', index: 'NUMERO_PRODUCTO', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('num-producto'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' }, width: 180 },
                { key: false, label: 'Cuenta', name: 'NUMERO_CUENTA', index: 'NUMERO_CUENTA', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('num-cuenta'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' }, width: 150 },
                { key: false, label: 'Estado', name: 'ESTADO_TXA', index: 'ESTADO_TXA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_estadotxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true }, width: 240 },
                { key: false, label: 'Fecha txa', name: 'FECHA_TXA', index: 'FECHA_TXA', editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' }, width: 210 },
                { key: false, label: 'Hora txa', name: 'HORA_TXA', index: 'HORA_TXA', editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "HH:mm:ss"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' hh:mm:ss' }, width: 100 },
                { key: false, label: 'Valor txa', name: 'VALOR_TXA', index: 'VALOR_TXA', editable: true, formatter: 'currency', formatoptions: { decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ " }, align: 'center', formoptions: { elmprefix: '(*) ' }, editrules: { edithidden: true, required: true, number: true, integer: true } },
                { key: false, label: 'Canal', name: 'CANAL_TXA', index: 'CANAL_TXA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_canaltxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true }, width: 110 },
                { key: false, label: 'Dispositivo', name: 'NOMBRE_DISPOSITIVO', index: 'NOMBRE_DISPOSITIVO', editable: true, width: 260, align: 'center', edittype: 'text', editrules: { required: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Tipo txa', name: 'TIPO_TXA', index: 'TIPO_TXA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_tipotxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true }, width: 260 },
                { key: false, label: 'Ciudad', name: 'CIUDAD_TXA', index: 'CIUDAD_TXA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_ciudadtxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Pais', name: 'PAIS_TXA', index: 'PAIS_TXA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_paistxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Respuesta', name: 'RESPUESTA_TXA', index: 'RESPUESTA_TXA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_resptxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Cta receptora', name: 'CUENTA_RECEPTORA', index: 'CUENTA_RECEPTORA', editable: true, edittype: 'text', align: 'center', editrules: { required: true, number: true, integer: true, custom: true, custom_func: function (val) { if (val.length === 11) return [true, null]; else return [false, 'Numero cuenta debe contener 11 digitos']; } }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Ip', name: 'IP_TXA', index: 'IP_TXA', editable: true, align: 'center', edittype: 'text', editrules: { required: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Riesgo', name: 'RIESGO_ALERTA', index: 'RIESGO_ALERTA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_riesgotxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Herramienta.', name: 'MODO_DETECCION', index: 'MODO_DETECCION', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_herrtxa' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Fecha ing.', name: 'FECHA_INGRESO', index: 'FECHA_INGRESO', hidden: true, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: 'Hora ing.', name: 'HORA_INGRESO', index: 'HORA_INGRESO', hidden: true, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "HH:mm:ss"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' hh:mm:ss' } },
                { key: false, label: 'Usuario', name: 'USUARIO_INGRESO', index: 'USUARIO_INGRESO', hidden: true, editable: true, align: 'center', formatter: 'text', editoptions: { defaultValue: function () { return $divData.data('usuario'); }, readonly: 'readonly' }, editrules: { edithidden: true, required: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: '', name: '__RequestVerificationToken', index: '__RequestVerificationToken', editable: true, hidden: true, editoptions: { defaultValue: function () { return $afToken.val(); } } }
            ],
            pager: jQuery('#pager-trx'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'ID_CLIENTE',
            sortorder: 'desc',
            postData: { sender: '.gr-trx', documento: null, producto: null, prod_numero: null, cuenta_numero: null },
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
            gridComplete: basesGridsEvents.trxGridCompleteEventHandler
        }).navGrid('#pager-trx', gridOptions.navBarOpts, gridOptions.trxAjaxOptions, gridOptions.trxAjaxOptions);

        $gridTransacciones.jqGrid('setGridWidth', $gridTransacciones.closest('.ui-jqgrid').parent().width(), true);
        /*
         * Instancia el tercer jqGrid de la pagina
         * Grid: "Novedades"
         * Selector: $('table#grid-novedades')
         */
        $gridNovedades.jqGrid({
            url: SIIGMA.UrlBase + 'bases/jqgriddispatcher',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, name: 'Id', index: 'Id', hidden: true, align: 'center' },
                { key: false, label: 'Documento', name: 'ID_CLIENTE', index: 'ID_CLIENTE', editable: true, hidden: true, align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('id-cliente'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Tipo Producto', name: 'TIPO_PRODUCTO', index: 'TIPO_PRODUCTO', editable: true, width: 80, align: 'center', hidden: true, editrules: { edithidden: true, required: true }, editoptions: { defaultValue: function () { return $divData.data('producto'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Producto', name: 'NUMERO_PRODUCTO', index: 'NUMERO_PRODUCTO', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('num-producto'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' }, width: 180 },
                { key: false, label: 'Cuenta', name: 'NUMERO_CUENTA', index: 'NUMERO_CUENTA', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('num-cuenta'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' }, width: 150 },
                { key: false, label: 'Causa desmarcación', name: 'CAUSA_DESMARCACION', index: 'CAUSA_DESMARCACION', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_causadesm' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true }, width: 240 },
                { key: false, label: 'Usuario desmarcación', name: 'USUARIO_DESMARCACION', index: 'USUARIO_DESMARCACION', editable: true, align: 'center', edittype: 'text', editrules: { required: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Empleado desmarcación', name: 'EMPLEADO_DESMARCACION', index: 'EMPLEADO_DESMARCACION', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_empleado' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Oficina', name: 'OFICINA', index: 'OFICINA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_oficina' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Observaciones', name: 'OBSERVACIONES', index: 'OBSERVACIONES', editable: true, edittype: 'textarea', editoptions: { rows: '3', cols: '30' }, hidden: true, align: 'center', editrules: { edithidden: true, required: false }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Fecha desbloqueo', name: 'FECHA_DESBLOQUEO', index: 'FECHA_DESBLOQUEO', hidden: false, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: 'Hora desbloqueo', name: 'HORA_DESBLOQUEO', index: 'HORA_DESBLOQUEO', editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "HH:mm:ss"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' hh:mm:ss' } },
                { key: false, label: 'Fecha creación', name: 'FECHA_CREACION', index: 'FECHA_CREACION', hidden: true, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); } }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: '', name: '__RequestVerificationToken', index: '__RequestVerificationToken', editable: true, hidden: true, editoptions: { defaultValue: function () { return $afToken.val(); } } }
            ],
            pager: jQuery('#pager-novedades'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'ID_CLIENTE',
            sortorder: 'desc',
            postData: { sender: '.gr-novedad', documento: null, producto: null, prod_numero: null, cuenta_numero: null },
            jsonReader: { root: 'rows', page: 'page', total: 'total', record: 'records', repeatitems: false, Id: '0' },
            multiselect: false,
            subGrid: true,
            subGridRowExpanded: basesGridsEvents.subGridRowExpandedEventHandler,
            gridComplete: basesGridsEvents.novedadesGridCompleteEventHandler
        }).navGrid('#pager-novedades', gridOptions.navBarOpts, gridOptions.novedadesAjaxOptions, gridOptions.novedadesAjaxOptions);

        $gridNovedades.jqGrid('setGridWidth', $gridNovedades.closest('.ui-jqgrid').parent().width(), true);
        /*
         * Instancia el cuarto jqGrid de la pagina
         * Grid: "Marcaciones"
         * Selector: $('table#grid-marcaciones')
         */
        $gridMarcaciones.jqGrid({
            url: SIIGMA.UrlBase + 'bases/jqgriddispatcher',
            datatype: 'json',
            mtype: 'GET',
            colModel: [
                { key: true, label: 'Id', name: 'Id', index: 'Id', hidden: true, align: 'center' },
                { key: false, label: 'Documento', name: 'ID_CLIENTE', index: 'ID_CLIENTE', editable: true, hidden: true, align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('id-cliente'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Tipo Producto', name: 'TIPO_PRODUCTO', index: 'TIPO_PRODUCTO', editable: true, width: 80, align: 'center', hidden: true, editrules: { edithidden: true, required: true }, editoptions: { defaultValue: function () { return $divData.data('producto'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Producto', name: 'NUMERO_PRODUCTO', index: 'NUMERO_PRODUCTO', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('num-producto'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' }, width: 180 },
                { key: false, label: 'Cuenta', name: 'NUMERO_CUENTA', index: 'NUMERO_CUENTA', editable: true, edittype: 'text', align: 'center', editrules: { edithidden: true, required: true, number: true, integer: true }, editoptions: { defaultValue: function () { return $divData.data('num-cuenta'); }, readonly: 'readonly' }, formoptions: { elmprefix: '(*) ' }, width: 150 },
                { key: false, label: 'Usuario conf.', name: 'USUARIO_CONFIRMACION', index: 'USUARIO_CONFIRMACION', editable: true, align: 'center', editoptions: { defaultValue: function () { return $divData.data('usuario'); } }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'CC confirm.', name: 'CC_CONFIRMACION', index: 'CC_CONFIRMACION', editable: true, align: 'center', editoptions: { defaultValue: function () { return $divData.data('usuario'); } }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Oficina', name: 'OFICINA_CONFIRMACION', index: 'OFICINA_CONFIRMACION', editable: true, align: 'center', editoptions: { dataUrl: 'bases/_makedropdown?t=t_oficina' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Fecha fraude', name: 'FECHA_FRAUDE', index: 'FECHA_FRAUDE', hidden: false, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); }, readonly: 'readonly' }, formatoptions: { newformat: 'd/m/Y' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: 'Valor', name: 'VALOR_FRAUDE', index: 'VALOR_FRAUDE', editable: true, formatter: 'currency', editrules: { edithidden: true, required: true, number: true, integer: true }, formatoptions: { decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ " }, align: 'center', formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Val. protegido', name: 'VALOR_PROTEGIDO', index: 'VALOR_PROTEGIDO', editable: true, formatter: 'currency', editrules: { edithidden: true, required: true, number: true, integer: true }, formatoptions: { decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ " }, align: 'center', formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Val. x recupe.', name: 'VALOR_POR_RECUPERAR', index: 'VALOR_POR_RECUPERAR', editable: true, formatter: 'currency', editrules: { edithidden: true, required: true, number: true, integer: true }, formatoptions: { decimalSeparator: ",", thousandsSeparator: ".", decimalPlaces: 0, prefix: "$ " }, align: 'center', formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Modalidad', name: 'MODALIDA', index: 'MODALIDA', editable: true, align: 'center', edittype: 'select', editoptions: { dataUrl: 'bases/_makedropdown?t=t_modalidad' }, formoptions: { elmprefix: '(*) ' }, editrules: { required: true } },
                { key: false, label: 'Usuario marc.', name: 'USUARIO_INGRESO', index: 'USUARIO_INGRESO', editable: true, align: 'center', formatter: 'text', editoptions: { defaultValue: function () { return $divData.data('usuario'); }, readonly: 'readonly' }, editrules: { edithidden: true, required: true }, formoptions: { elmprefix: '(*) ' } },
                { key: false, label: 'Fecha marcac.', name: 'FECHA_INGRESO', index: 'FECHA_INGRESO', hidden: true, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); }, readonly: 'readonly' }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: 'Hora marcac.', name: 'HORA_INGRESO', index: 'HORA_INGRESO', editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "HH:mm:ss"); }, readonly: 'readonly' }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' hh:mm:ss' } },
                { key: false, label: 'Fecha creac.', name: 'FECHA_CREACION', index: 'FECHA_CREACION', hidden: true, editable: true, formatter: 'date', editoptions: { defaultValue: function () { return $.format.date(new Date(), "dd/MM/yyyy"); }, readonly: 'readonly' }, formatoptions: { newformat: 'd/m/Y H:i:s' }, editrules: { edithidden: true, required: true }, sorttype: 'text', align: 'center', formoptions: { elmprefix: '(*) ', elmsuffix: ' dd/mm/aaaa' } },
                { key: false, label: '', name: '__RequestVerificationToken', index: '__RequestVerificationToken', editable: true, hidden: true, editoptions: { defaultValue: function () { return $afToken.val(); } } }
            ],
            pager: jQuery('#pager-marcaciones'),
            rowNum: 10,
            rowList: [10, 20, 30, 40],
            height: 'auto',
            viewrecords: true,
            sortname: 'ID_CLIENTE',
            sortorder: 'desc',
            postData: { sender: '.gr-fraude', documento: null, producto: null, prod_numero: null, cuenta_numero: null },
            multiselect: false,
            gridComplete: basesGridsEvents.fraudeGridCompleteEventHandler
        }).navGrid('#pager-marcaciones', gridOptions.navBarOpts, gridOptions.marcacionesAjaxOptions, gridOptions.marcacionesAjaxOptions);

        $gridMarcaciones.jqGrid('setGridWidth', $gridMarcaciones.closest('.ui-jqgrid').parent().width(), true);
    },
    /*
     * Método que ejecuta el trigger de jqGrid que refresca y actualiza la información del primer jqGrid "Bloqueos"
     * Este metodo primero limpia los datos que existan en todos los grid para evitar confusión con la información nueva
     * y luego ejecuta el metodo "reloadGrid" de la API de jqGrid para que el grid envíe nuevamente la petición AJAX al
     * servidor y actulice la información del grid
     */
    reloadGridBloqueos = function () {
        $gridBloqueos.jqGrid('clearGridData')
        $gridTransacciones.jqGrid('clearGridData')
        $gridNovedades.jqGrid('clearGridData')
        $gridMarcaciones.jqGrid('clearGridData')
        
        var postDataValues = $gridBloqueos.jqGrid('getGridParam', 'postData');
        postDataValues['documento'] = $txtDoc.val();
        $gridBloqueos.jqGrid().setGridParam({ postData: postDataValues, page: 1 }).trigger('reloadGrid');
    },
    /*
     * Agrega nueva funcionalidad a los grids, haciendo un llamado a la API de jqGrid para agregar un boton adicional a la
     * navBar para que permita mostrar u ocultar columnas en los grids.
     */
    addColsButtons = function () {
        $gridBloqueos.jqGrid('navButtonAdd', '#pager-bloqueos', { caption: "Mostrar/ocultar columnas", title: "Mostrar/ocultar columnas", onClickButton: function () { $gridBloqueos.jqGrid('columnChooser', { done: function () { utils.setGridWidth($gridBloqueos); } }); } });
        $gridTransacciones.jqGrid('navButtonAdd', '#pager-trx', { caption: "Mostrar/ocultar columnas", title: "Mostrar/ocultar columnas", onClickButton: function () { $gridTransacciones.jqGrid('columnChooser', { done: function () { utils.setGridWidth($gridTransacciones); } }); } });
        $gridNovedades.jqGrid('navButtonAdd', '#pager-novedades', { caption: "Mostrar/ocultar columnas", title: "Mostrar/ocultar columnas", onClickButton: function () { $gridNovedades.jqGrid('columnChooser', { done: function () { utils.setGridWidth($gridNovedades); } }); } });
        $gridMarcaciones.jqGrid('navButtonAdd', '#pager-marcaciones', { caption: "Mostrar/ocultar columnas", title: "Mostrar/ocultar columnas", onClickButton: function () { $gridMarcaciones.jqGrid('columnChooser', { done: function () { utils.setGridWidth($gridMarcaciones); } }); } });
    },
    /*
     * Colección de funciones y metodos que son enlazados a los diferentes eventos de los grid
     * al momento de su inicialización. En la definición de las opciones de los grids se hace la
     * referencia a estos metodos.
     */
    basesGridsEvents = {
        subGridRowExpandedEventHandler: function (subgrid_id, row_id) {
            var $data = $(this).jqGrid('getRowData', row_id);
            $('#' + subgrid_id).html('<div style="padding: 5px 5px;"><b>Observaciones:</b> ' + $data.OBSERVACIONES + '</div>');
        },
        bloqueosGridCompleteEventHandler: function () {
            utils.setGridWidth($gridBloqueos);
            $divData.data('id-cliente', '');
            $divData.data('producto', '');
            $divData.data('num-producto', '');
            $divData.data('num-cuenta', '');
            $('#hidID_CLIENTE').val('');
            $('#hidNUMERO_CUENTA').val('');
            $('#hidNUMERO_PRODUCTO').val('');
            $('#hidTIPO_PRODUCTO').val('');
        },
        bloqueosBeforeSelectRowEventHandler: function (rowid, e) {
            var $data = $(this).jqGrid('getRowData', rowid);
            if (!$.isNumeric($data.ID_CLIENTE))
                return;

            var postDataValues = {
                documento: $data.ID_CLIENTE,
                producto: $data.TIPO_PRODUCTO,
                prod_numero: $data.NUMERO_PRODUCTO,
                cuenta_numero: $data.NUMERO_CUENTA
            };

            $gridTransacciones.jqGrid().setGridParam({ postData: postDataValues, page: 1 }).trigger('reloadGrid');
            $gridNovedades.jqGrid().setGridParam({ postData: postDataValues, page: 1 }).trigger('reloadGrid');
            $gridMarcaciones.jqGrid().setGridParam({ postData: postDataValues, page: 1 }).trigger('reloadGrid');

            $divData.data('id-cliente', $data.ID_CLIENTE);
            $divData.data('producto', $data.TIPO_PRODUCTO);
            $divData.data('num-producto', $data.NUMERO_PRODUCTO);
            $divData.data('num-cuenta', $data.NUMERO_CUENTA);

            $('#hidID_CLIENTE').val($data.ID_CLIENTE);
            $('#hidNUMERO_CUENTA').val($data.NUMERO_CUENTA);
            $('#hidNUMERO_PRODUCTO').val($data.NUMERO_PRODUCTO);
            $('#hidTIPO_PRODUCTO').val($data.TIPO_PRODUCTO);

            $('label#lblMasivoTrx').removeAttr('disabled');
            $('button#btnCargar').removeAttr('disabled');
            $('input#MasivoTrx').removeAttr('disabled');
        },
        trxGridCompleteEventHandler: function () { utils.setGridWidth($gridTransacciones); },
        novedadesGridCompleteEventHandler: function () { utils.setGridWidth($gridNovedades); },
        fraudeGridCompleteEventHandler: function () { utils.setGridWidth($gridMarcaciones); },
        defaultCallback: function (response, postdata, formid) {
            if (response.responseJSON.success) {
                toastr['success']('El registro ha sido almacenado correctamente', 'Correcto');
            } else {
                toastr['error']('Se ha presentado un error al procesar la solicitud, por favor contacte al administrador.', 'Error!');
            }
        },
        sortColCallback: function (index, iCol, sortorder) {
            $divData.data('id-cliente', '');
            $divData.data('producto', '');
            $divData.data('num-producto', '');
            $divData.data('num-cuenta', '');

            $('#hidID_CLIENTE').val('');
            $('#hidNUMERO_CUENTA').val('');
            $('#hidNUMERO_PRODUCTO').val('');
            $('#hidTIPO_PRODUCTO').val('');

            $('label#lblMasivoTrx').attr('disabled', 'disabled');
            $('button#btnCargar').attr('disabled', 'disabled');
            $('input#MasivoTrx').attr('disabled', 'disabled');
        }
    },
    /*
     * Coleccion de funciones y metodos utiles para la pagina.
     * - Modificar el tamaño del grid para que se ajuste al contenedor principal
     */
    utils = {
        setGridWidth: function (element) {
            var $width = element.closest('.ui-jqgrid').parent().width();
            element.jqGrid('setGridWidth', $width, true);
        }
    },
    /*
     * Coleccion de funciones y metodos que soportan los diferentes formatos de columnas
     * para los datos cargados en los grid, estos son formatos personalizados para extender
     * las funcionalidades de los formateadores predeterminados de jqGrid.
     */
    formats = {
        trxDate: function (cellvalue, options, rowObject) {
            var f = $.format.date(new Date(parseInt(rowObject.FECHA_TXA.substr(6))), "dd/MM/yyyy");
            var h = $.format.date(new Date(parseInt(rowObject.HORA_TXA.substr(6))), "HH:mm:ss");
            return f + ' ' + h;
        },
        novedadesDate: function (cellvalue, options, rowObject) {
            var f = $.format.date(new Date(parseInt(rowObject.FECHA_DESBLOQUEO.substr(6))), "dd/MM/yyyy");
            var h = $.format.date(new Date(parseInt(rowObject.HORA_DESBLOQUEO.substr(6))), "HH:mm:ss");
            return f + ' ' + h;
        },
        fraudeDate: function (cellvalue, options, rowObject) {
            var f = $.format.date(new Date(parseInt(rowObject.FECHA_INGRESO.substr(6))), "dd/MM/yyyy");
            var h = $.format.date(new Date(parseInt(rowObject.HORA_INGRESO.substr(6))), "HH:mm:ss");
            return f + ' ' + h;
        },
    },
    /*
     * Coleccion de objetos que contienen las definiciones de las opciones para los diferentes
     * eventos de "Edit" y "Add" de los grids, en la definicion del grid de hace la referencia
     * hacia la coleccion.
     */
    gridOptions = {
        navBarOpts: { edit: true, add: true, del: false, search: false, refresh: true },
        bloqueosAjaxOptions: {
            url: SIIGMA.UrlBase + 'bases/guardarbloqueo', closeAfterEdit: true, closeAfterAdd: true, jqModal: true, bottominfo: 'Los campos marcados con asterísco (*) son obligatorio', top: 300, left: 300,
            closeOnEscape: true, editData: { '__RequestVerificationToken': $afToken.val() }, viewPagerButtons: false, afterComplete: basesGridsEvents.defaultCallback
        },
        trxAjaxOptions: {
            url: SIIGMA.UrlBase + 'bases/guardartrx', closeAfterEdit: true, closeAfterAdd: true, jqModal: true, bottominfo: 'Los campos marcados con asterísco (*) son obligatorio', top: 300, left: 300,
            closeOnEscape: true, editData: { '__RequestVerificationToken': $afToken.val() }, viewPagerButtons: false, afterComplete: basesGridsEvents.defaultCallback
        },
        novedadesAjaxOptions: {
            url: SIIGMA.UrlBase + 'bases/guardarnovedad', closeAfterEdit: true, closeAfterAdd: true, jqModal: true, bottominfo: 'Los campos marcados con asterísco (*) son obligatorio', top: 300, left: 300,
            closeOnEscape: true, editData: { '__RequestVerificationToken': $afToken.val() }, viewPagerButtons: false, afterComplete: basesGridsEvents.defaultCallback
        },
        marcacionesAjaxOptions: {
            url: SIIGMA.UrlBase + 'bases/guardarfraude', closeAfterEdit: true, closeAfterAdd: true, jqModal: true, bottominfo: 'Los campos marcados con asterísco (*) son obligatorio', top: 300, left: 300,
            closeOnEscape: true, editData: { '__RequestVerificationToken': $afToken.val() }, viewPagerButtons: false, afterComplete: basesGridsEvents.defaultCallback
        }
    },
    /*
     * Definición único método público de la API, el único método que es visible fuera del objeto jQuery
     * y que internamente hace el llamado a los eventos invocables del objeto.
     */
    init = function () {
        basesInstanciarEventos();
        instanceControls();
        addColsButtons();
    };

    return {
        init: init
    }
}());