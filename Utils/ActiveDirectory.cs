﻿using System;
using System.DirectoryServices;

namespace SIIGMA.Utils
{
    public static class ActiveDirectory
    {
        public static string GetFullUsername(string username)
        {
            if(string.IsNullOrEmpty(username))
                throw new ArgumentNullException("username", string.Format("[ERR-001]: El nombre de usuario no puede ser nulo; Clase:<{0}>, Metodo:<{1}>", typeof(ActiveDirectory).Name, nameof(ActiveDirectory.GetFullUsername)));

            DirectoryEntry de = new DirectoryEntry(string.Format("WinNT://{0}/{1}", Environment.UserDomainName, username));
            return de.Properties["fullName"].Value.ToString();
        }
    }
}