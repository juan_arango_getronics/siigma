﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Dynamic;

namespace SIIGMA.Utils
{
    public static class Database
    {
        public static IEnumerable<dynamic> CollectionFromSql(this DbContext database, string query, Dictionary<string, object> parameters)
        {
            using(var cmd = database.Database.Connection.CreateCommand())
            {
                cmd.CommandText = query;
                if(cmd.Connection.State != System.Data.ConnectionState.Open)
                    cmd.Connection.Open();

                foreach(KeyValuePair<string, object> param in parameters)
                {
                    DbParameter dbp = cmd.CreateParameter();
                    dbp.ParameterName = param.Key;
                    dbp.Value = param.Value;
                    cmd.Parameters.Add(dbp);
                }

                using(var dr = cmd.ExecuteReader())
                {
                    while(dr.Read())
                    {
                        var dataRow = GetDataRow(dr);
                        yield return dataRow;
                    }
                }
            }
        }

        private static dynamic GetDataRow(DbDataReader dr)
        {
            var dataRow = new ExpandoObject() as IDictionary<string, object>;
            for(var fieldCount = 0; fieldCount < dr.FieldCount; fieldCount++)
            {
                if(dr.GetName(fieldCount).Contains("LLAVE") || dr.GetName(fieldCount).Contains("SORTINDEX"))
                    continue;
                dataRow.Add(dr.GetName(fieldCount), dr[fieldCount]);
            }
            return dataRow;
        }
    }
}