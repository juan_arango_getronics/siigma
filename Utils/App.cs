﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIIGMA.Utils
{
    public static class App
    {
        public static DateTime ExtraerFecha(object fecha)
        {
            if(fecha == null)
                throw new ArgumentNullException("fecha");

            string fechaStr = Convert.ToString(fecha);

            var tamano = fechaStr.Length;

            string annio = string.Empty;
            string mes = string.Empty;
            string dia = string.Empty;
            string hora = string.Empty;
            string min = string.Empty;
            string seg = string.Empty;

            switch(tamano)
            {
                case 8:
                    if(fechaStr.Contains(":"))
                    {
                        annio = DateTime.Now.Year.ToString();
                        mes = DateTime.Now.Month.ToString();
                        dia = DateTime.Now.Day.ToString();
                        hora = fechaStr.Substring(0,2);
                        min = fechaStr.Substring(3,2);
                        seg = fechaStr.Substring(6,2);
                    }
                    else
                    {
                        annio = fechaStr.Substring(0, 4);
                        mes = fechaStr.Substring(4, 2);
                        dia = fechaStr.Substring(6, 2);
                        hora = "00";
                        min = "00";
                        seg = "00";
                    }
                    break;
                default:
                    break;
            }

            return DateTime.Parse(string.Format("{0}-{1}-{2} {3}:{4}:{5}", annio, mes, dia, hora, min, seg));
        }
    }
}